const restify = require('restify');
const _ = require('lodash');
const axios = require('axios');
const moment = require('moment');
const Router = require('restify-router').Router;
const fs = require('fs');
const path = require('path');
const shortid = require('shortid');
const promise = require('bluebird');
const routerInstance = new Router();
const models = require('./models');
const sm2 = require('./lib/sm2');
const jwtAuth = require('./auth/jwt');
const env = process.env.NODE_ENV || 'development';
const config = require('../config/' + env);
const mime = require('mime');

routerInstance.post('/otherSets', jwtAuth, saveSetByOther);
routerInstance.get('/sets', getAllSets);
routerInstance.get('/sets/byUser', jwtAuth, getCurrentSets);
routerInstance.get('/sets/user/:userId', jwtAuth, getSetsByUser);
routerInstance.get('/sets/:setId', getSetById);
routerInstance.get('/sets/title/:setTitle', getSetByTitle);
routerInstance.post('/sets', jwtAuth, saveStudySets);
routerInstance.post('/sets/upload', jwtAuth, handleUploadFile);
routerInstance.post('/sets/uploadText', jwtAuth, importText);
routerInstance.post('/sets/:setId', jwtAuth, saveStudySets);
routerInstance.del('/sets/:setId', jwtAuth, deleteSet);
routerInstance.get('/sets/:setId/learn', jwtAuth, startLearn);
routerInstance.get('/sets/:setId/progress', jwtAuth, getMyProgress);
routerInstance.get('/sets/:setId/nextcard', jwtAuth, getNextFlashcard);
routerInstance.post('/progress/:progressId', jwtAuth, saveProgress);
routerInstance.post('/test/:setId', jwtAuth, saveTest);
routerInstance.get('/test/:setId', jwtAuth, getTestResult);
routerInstance.post('/game', jwtAuth, saveGameResult);
routerInstance.get('/history', jwtAuth, getHistoryGame);
routerInstance.post('/search', jwtAuth, searchSetsByOther);

promise.config({
    cancellation: true
});

const instance = axios.create({
    baseURL: 'https://api.quizlet.com/2.0/',
    params: {
        whitespace: 1,
        client_id: 'bY3TqAuDtb'
    }
});

const IMAGE_PATH = '/images/';

module.exports = routerInstance;

function searchSetsByOther(req, res, next) {
    var textSearch = req.body.textSearch;
    instance.get('search/sets', {
        params: {
            q: textSearch
        }
    }).then(function(response) {
        res.send({data: response.data.sets});
    }).catch(function (error) {
        return next(new restify.InternalServerError(error));
    });
}

function saveSetByOther(req, res, next) {
    instance.get('sets/' + req.body.setId)
        .then(function(response) {
            _.forEach(response.data.terms, (value, key) => {
               response.data.terms[key] = _.omit(value, ['image', 'rank']);
            });
            let s = new models.StudySet({
                title: response.data.title,
                description: (response.data.description == "") ? "Default description" : response.data.description,
                sets: response.data.terms,
                createdBy: req.loggedUser.id,
                termLanguage: response.data.lang_terms,
                definitionLanguage: response.data.lang_definitions,
                image: "/photo_holder.png",
                public: true
            });
            s.save(function (err) {
                if (err) {
                    return next(err);
                }
                return res.send({status: true});
            })
        })
        .catch(function (error) {
            return next(new restify.InternalServerError(error));
        });
}

function getAllSets(req, res, next) {
    models.StudySet.find({public: true})
        .populate('createdBy')
        .exec(function (err, sets) {
        if (err) {
            return next(new restify.InternalServerError(err));
        }
        res.send({status: true, data: {sets: sets}});
    })
}

function getSetById(req, res, next) {
    var id = req.params.setId;
    if (id) {
        models.StudySet.findById(id).exec(function (err, doc) {
            if (err) {
                return next(new restify.InternalServerError(err));
            }
            if (!doc) {
                return next(new restify.NotFoundError());
            }
            return res.send({status: true, data: doc});
        });
    } else {
        return next(new restify.NotFoundError());
    }
}

function getSetByTitle(req, res, next) {
    var title = req.params.setTitle;
    var regex = new RegExp(title, 'i');
    if(title) {
        models.StudySet.find({title: regex, public: true}).populate('createdBy').exec(function(err, doc) {
            if (err) {
                return next(new restify.InternalServerError(err));
            }
            if (!doc) {
                return next(new restify.NotFoundError());
            }
            return res.send({status: true, data: doc});
        });
    } else {
        return next(new restify.NotFoundError());
    }
}

function deleteSet(req, res, next) {
    var id = req.params.setId;
    if (id) {
        models.StudySet.findByIdAndRemove(id).exec(function (err) {
            if (err) {
                return next(new restify.InternalServerError(err));
            }
            return res.send({status: true});
        })
    } else {
        return next(new restify.NotFoundError());
    }
}

function saveStudySets(req, res, next) {
    var studySetId = req.body.setId;
    var title = req.body.title;
    var description = req.body.description;
    var sets = req.body.sets;
    var termLanguage = req.body.termLanguage;
    var definitionLanguage = req.body.definitionLanguage;
    var image = req.body.image;
    var public = req.body.public;
    if (studySetId) {
        models.StudySet.findById(studySetId).exec(function (err, set) {
            if (err) {
                return next(err);
            }
            if (!set) {
                return next(new restify.NotFoundError());
            }
            set.title = title;
            set.description = description;
            set.sets = sets;
            set.termLanguage = termLanguage;
            set.definitionLanguage = definitionLanguage;
            set.image = image;
            set.public = public;
            set.save(function (err) {
                if (err) {
                    return next(err);
                }
                return res.send({status: true});
            })
        });
    } else {
        var s = new models.StudySet({
            title: title,
            description: description,
            sets: sets,
            createdBy: req.loggedUser.id,
            termLanguage: termLanguage,
            definitionLanguage: definitionLanguage,
            image: image,
            public: public
        });
        s.save(function (err) {
            if (err) {
                return next(err);
            }
            return res.send({status: true});
        })
    }
}

function handleUploadFile(req, res, next) {
    if (req.files) {
        var ext = path.extname(req.files.file0.name);
        var filename = shortid.generate();
        var outputPath = config.uploadFolder + filename + ext;
        var wstream = fs.createWriteStream(outputPath);
        fs.readFile(req.files.file0.path, function (err, data) {
            if (err) {
                return next(new restify.InternalServerError(err));
            }
            wstream.on('finish', function (err) {
                if (err) {
                    return next(new restify.InternalServerError(err));
                }
                res.send({status: true, data: {filename: IMAGE_PATH + filename + ext}});
            });
            wstream.write(data);
            wstream.end();
        });
    }
}

function importText(req, res, next) {
    if (req.files) {
        fs.readFile(req.files.file0.path, 'utf8', function(err, data) {
            if (err) {
                return next(new restify.InternalServerError(err));
            }
            let dataUpload = [];
            data.split('\n').forEach((value) => {
                if(value !== '') {
                    var split = value.split(',');
                    const objectArray = {id: shortid.generate(), term: split[0], definition: split[1]};
                    dataUpload.push(objectArray);
                }
            });
            res.send({data: dataUpload});
        });
    }
}

function getCurrentSets(req, res, next) {
    if (req.loggedUser) {
        models.StudySet.find({createdBy: req.loggedUser.id}).exec(function (err, sets) {
            if (err) {
                return next(new restify.InternalServerError(err));
            }
            res.send({status: true, data: {sets: sets}});
        })
    } else {
        return next(new restify.NotFoundError());
    }
}

function getSetsByUser(req, res, next) {
    if(req.params.userId) {
        models.StudySet.find({createdBy: req.params.userId, public: true}).exec((err, sets) => {
           if(err) return next(err);
           res.send({status: true, data: sets});
        });
    }
}

function startLearn(req, res, next) {
    var userId = req.loggedUser.id;
    var setId = req.params.setId;
    if (!userId || !setId){
        return next(new restify.BadRequestError('Missing parameters'));
    }
    var studySetP = models.StudySet.findById(setId).exec();
    var userProgressP = models.UserProgress.find({user: userId, studySet: setId}).exec();
    var p = promise.all([studySetP, userProgressP])
        .then(function ([studySet, userProgress]) {
            var arr = [];
            studySet.sets.forEach(function (s) {
                var filtered = userProgress.filter(function (p) {
                    return s.id == p.pair;
                });
                if (!filtered.length){
                    arr.push(new models.UserProgress({studySet: studySet.id, user: userId, pair: s.id}))
                }
            });
            if (arr.length){
                return models.UserProgress.insertMany(arr);
            } else {
                p.cancel(); // not chaining to next promise then
                return res.send({status: true});
            }
        }).then(function (bulk) {
            return res.send({status: true})
        }).catch(function (err) {
            return next(err);
        });
}

function getMyProgress(req, res, next) {
    var userId = req.loggedUser.id;
    var setId = req.params.setId;
    if (!userId || !setId){
        return next(new restify.BadRequestError('Missing parameters'));
    }
    var progressP = models.UserProgress
        .find({user: userId, studySet: setId})
        .sort({updatedAt: 1})
        .exec();
    var studySetP = models.StudySet.findById(setId).exec();
    promise.all([progressP, studySetP]).then(function ([progress, studySet]) {
        var arr = [];
        progress.forEach(function (item) {
            var isSameHour = moment(item.updatedAt).add(item.interval, "hours").isSame(moment(), "hour");
            var isBefore = moment(item.updatedAt).add(item.interval, "hours").isBefore(moment());
            if (isSameHour || isBefore){
                var filtered = studySet.sets.filter(function (s) {
                    return s.id == item.pair;
                });
                var obj = {
                    id: item.id,
                    interval: item.interval,
                    pair: item.pair,
                    ef: item.ef,
                    updatedAt: item.updatedAt
                }
                if (filtered.length){
                    obj.pairObject = filtered[0];
                }
                arr.push(obj);
            }
        });
        return res.send({status: true, data: {progress: arr, studySet: studySet}});
    }).catch(function (err) {
        return next(err);
    })
}

function getNextFlashcard(req, res, next) {
    var userId = req.loggedUser.id;
    var setId = req.params.setId;
    if (!userId || !setId){
        return next(new restify.BadRequestError('Missing parameters'));
    }
    var progressP = models.UserProgress
        .find({user: userId, studySet: setId})
        .sort({updatedAt: 1, interval: 1})
        .exec();
    var studySetP = models.StudySet.findById(setId).exec();
    promise.all([progressP, studySetP]).then(function ([progress, studySet]) {
        var arr = [];
        progress.forEach(function (item) {
            var isSameHour = moment(item.updatedAt).add(item.interval, "hours").isSame(moment(), "hour");
            var isBefore = moment(item.updatedAt).add(item.interval, "hours").isBefore(moment());
            if (isSameHour || isBefore){
                var filtered = studySet.sets.filter(function (s) {
                    return s.id == item.pair;
                });
                var obj = {
                    id: item.id,
                    pair: item.pair,
                    updatedAt: item.updatedAt,
                    scheduleAt: moment(item.updatedAt).add(item.interval, "hours")
                };
                if (filtered.length){
                    obj.pairObject = filtered[0];
                }
                arr.push(obj);
            }
        });
        arr.sort(function (a,b) {
            if (a.scheduleAt.isBefore(b.scheduleAt)){
                return -1;
            } else if (a.scheduleAt.isAfter(b.scheduleAt)) {
                return 1;
            }
            return 0;
        });
        return res.send({status: true, data: arr});

    }).catch(next);
}

function saveProgress(req, res, next) {
    var progressId = req.params.progressId;
    var quality = req.body.quality;
    var progressP = models.UserProgress.findById(progressId).exec();
    progressP.then(function (progress) {
        var ret = new sm2(progress.ef, quality);
        if(quality >= 3) {
            progress.ef = ret.getNewEFactor();
            progress.interval = ret.getNextInterval(progress.n);
            progress.n ++;
        } else {
            progress.interval = 1;
            progress.n = 1;
        }
        progress.updatedAt = Date.now();
        return progress.save();
    }).then(function (p) {
        return res.send({status: true, data: p});
    }).catch(next);
}

function saveTest(req, res, next) {
    var setId = req.params.setId;
    var incorrect = req.body.incorrect;
    var correct = req.body.correct;
    if (!setId) {
        return next(new restify.BadRequestError('Missing parameters'));
    }

    var currUser = models.User.findById(req.loggedUser.id).exec();
    currUser.then(function (user) {
        var test = {correct: correct, incorrect: incorrect, studySet: setId};
        user.testResults.push(test);
        return user.save();
    }).then(function (doc) {
        var filtered = doc.testResults.filter(function (item) {
            return item.studySet == setId;
        });
        return res.send({status: true, data: filtered});
    }).catch(next);
}

function getTestResult(req, res, next) {
    var setId = req.params.setId;
    var currUser = models.User.findById(req.loggedUser.id).exec();
    currUser.then(function (user) {
        var filtered = user.testResults.filter(function (item) {
            return item.studySet == setId;
        });
        return res.send({status: true, data: filtered});
    }).catch(next);
}

function saveGameResult(req, res, next) {
    models.HistoryGame.findOne({ $or: [
        {'player1.id': req.loggedUser.id, 'player2.id': req.params.toId},
        {'player1.id': req.params.toId, 'player2.id': req.loggedUser.id}
    ]}).exec((err, doc) => {
        if(err) {
            return next(new restify.InternalServerError(err));
        }
        if(doc !== null) {
            if(doc.player1.id == req.loggedUser.id) {
                models.HistoryGame.update({_id: doc._id},{ $inc: { "player1.win" : 1 }}).exec((err, doc) => {
                    if(err) return next(err);
                    return res.send({status: true});
                });
            } else {
                models.HistoryGame.update({_id: doc._id},{ $inc: { "player2.win" : 1 }}).exec((err, doc) => {
                    if(err) return next(err);
                    return res.send({status: true});
                });
            }
        } else {
            let data = {
                player1: {id: req.loggedUser.id, win: 1},
                player2: {id: req.params.toId, win:0 }
            };
            models.HistoryGame.create(data, (err, doc) =>{
               if(err) return next(err);
               return res.send({status: true});
            });
        }

    });
}

function getHistoryGame(req, res, next) {
    models.HistoryGame.find({$or: [{'player1.id': req.loggedUser.id},{'player2.id': req.loggedUser.id}]})
        .populate('player1.id')
        .populate('player2.id')
        .sort({created: -1})
        .limit(5)
        .exec((err, history) => {
            if(err) return next(err);
            let result = [];
            for(let i = 0; i < history.length; i++) {
                let data = {};
                if (history[i].player1['id']['_id'] == req.loggedUser.id) {
                    data['player1'] = history[i].player2;
                    data['player2'] = history[i].player1;
                } else {
                    data = history[i];
                }
                result.push(data);
            }
            return res.send({status: true, data: result});
        });
}

