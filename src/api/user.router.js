var restify = require('restify');
var jwt = require('jsonwebtoken');
var Router = require('restify-router').Router;
var routerInstance = new Router();
var models = require('./models');
var jwtAuth = require('./auth/jwt');
const nodemailer = require('nodemailer');
routerInstance.get('/', jwtAuth, doIndex);
routerInstance.post('/auth', doAuthentication);
routerInstance.get('/user/signout', doSignout);
routerInstance.get('/user/:userId', jwtAuth, getUserById);
routerInstance.post('/user', jwtAuth, editUser);
routerInstance.post('/user/:userId', jwtAuth, editUser);
routerInstance.get('/user', jwtAuth, getListUserOnline);
routerInstance.get('/message', jwtAuth, getAllMessage);
routerInstance.post('/message', jwtAuth, saveMessage);
routerInstance.post('/contact', sendContactToEmail);
let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'tranbaquan1511@gmail.com',
        pass: 'bynnckt95'
    }
});
module.exports = routerInstance;

function doIndex(req, res, next){
    res.send({status: true, data: req.loggedUser});
}

function doAuthentication(req, res, next){
    if (req.body && req.body.email){
        models.User.findOne({email: req.body.email}).exec(function(err, doc){
            if (err) {
                return next(new restify.InternalServerError());
            }
            if (!doc){
                // create new user profile
                let userJson;
                if (req.body.googleId){
                    userJson = {socialId: req.body.googleId, email: req.body.email, name: req.body.name, photo: req.body.imageUrl};
                } else {
                    userJson = {socialId: req.body.id, email: req.body.email, name: req.body.name, photo: req.body.picture.data.url};
                }
                let user = new models.User(userJson);
                user.save(function(err, doc){
                    if (err) {
                        return next(new restify.InternalServerError());
                    }
                    let userToEncode = {email: userJson.email, name: userJson.name};
                    const quizToken = jwt.sign(userToEncode, "quantb", {
                        expiresIn: '30 days'
                    });
                    return res.send({status: true, data: {loggedUser: doc, quizToken: quizToken}});
                })
            } else {
                let userToEncode = {email: doc.email, name: doc.name};
                const quizToken = jwt.sign(userToEncode, "quantb", {
                    expiresIn: '30 days'
                });
                return res.send({status: true, data: {loggedUser: doc, quizToken: quizToken}});
            }
        });
    }
}

function doSignout(req, res, next){
    return res.send({status: true});
}

function getUserById(req, res, next) {
    let id = req.params.userId;
    if(id) {
        models.User.findById(id)
            .populate('listFriends')
            .exec(function(err, user) {
            if(err) {
                return next(new restify.InternalServerError(err));
            }
            if(!user) {
                return next(new restify.NotFoundError());
            }
            return res.send({status: true, data: user});
        })
    } else {
        return next(new restify.NotFoundError());
    }
}

function editUser(req, res, next) {
    let userId = req.body.userId;
    let changedName = req.body.name;
    if(userId) {
        models.User.findByIdAndUpdate(userId, {$set: {name: changedName}}, {new: true}, function(err, user) {
            if(err) {
                return res.send({error: err});
            }
            res.send({status: true});
        });
    } else {
        res.send({status: 'Cannot find userId'});
    }
}

function getListUserOnline(req, res, next) {
    models.User.find({$and : [{online: true}, {_id: { $nin : [req.loggedUser._id]}} ]}).exec(function(err, users) {
        if(err) {
            return res.send({error: err});
        }
        res.send({status: 'true', data: users});
    });
}

function getAllMessage(req, res, next) {
    models.Message.find({}).populate('userId').exec(function(err, listsMessage) {
       if(err) {
           return next(err);
       }
       res.send({status: 'true', data: listsMessage});
    });
}

function saveMessage(req, res, next) {
    let message = req.body.message;
    var m = new models.Message({
        userId: req.loggedUser._id,
        message: message
    });
    m.save(function(err) {
        if(err) return next(err);
        res.send({status: true});
    })

}

function sendContactToEmail(req, res, next) {
    let mailOptions = {
        from: req.body.name,
        to: 'zzbynzz@gmail.com',
        subject: req.body.name + ' sent you a message of contact in Uet Flashcard',
        text: req.body.message + '(' + req.body.email + ')'
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return next(error);
        }
        res.send({status: true, data: info});
    });
}