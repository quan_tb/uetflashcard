var restify = require('restify');
var jwt = require('jsonwebtoken');
var Router = require('restify-router').Router;
var routerInstance = new Router();
var models = require('./models');
var jwtAuth = require('./auth/jwt');

routerInstance.get('/request', jwtAuth, getFriendRequest);
routerInstance.post('/request/create', jwtAuth, createFriendRequest);
routerInstance.get('/request/checkPending/:toId', jwtAuth, checkPendingRequest);
routerInstance.post('/request/accept', jwtAuth, acceptFriendRequest);
routerInstance.post('/request/ignore', jwtAuth, ignoreFriendRequest);
routerInstance.post('/request/unfriend', jwtAuth, unfriend);
/*routerInstance.get('/friend/:user/:friend', jwtAuth, unfriend);*/
module.exports = routerInstance;

function createFriendRequest(req, res, next) {
    var friend = new models.Friend({
        from: req.body.fromId,
        to: req.body.toId
    });
    friend.save(function (err) {
        if(err) return next(err);
        return res.send({status: true});
    });
}

function checkPendingRequest(req, res, next) {
    models.Friend.findOne({ $or: [
            { $and : [{from: req.loggedUser._id}, {to: req.params.toId}] },
            { $and : [{from: req.params.toId}, {to: req.loggedUser._id}] }
        ]})
        .exec(function(err, doc) {
            if(!doc || doc.length == 0) res.send({loggedId: req.loggedUser._id});
            if(!doc.confirmed) {
                res.send({loggedId: req.loggedUser._id, isFriend: 'pending'});
            } else {
                res.send({loggedId: req.loggedUser._id, isFriend: 'friend'});
            }
        });
}

function getFriendRequest(req, res, next) {
    models.Friend.find({ $and: [{confirmed: false}, {to: req.loggedUser._id}] })
        .populate('from')
        .exec(function(err, friends) {
            if(err) return next(err);
            var result = [];
            for(var i=0; i < friends.length; i++) {
                result.push(friends[i].from);
            }
            res.send(result);
        });
}

function acceptFriendRequest(req, res, next) {
    models.Friend.findOneAndUpdate({ $and: [{from: req.params.fromId}, {to: req.loggedUser._id}]}, { $set: {confirmed: true}},
        function(err, friend) {
            if(err) return next(err);
            console.log(err);
            // add friend to
            models.User.findOne({_id: req.loggedUser._id}, function(err, user) {
                if(err) return next(err);
                if(user.listFriends.indexOf(req.params.fromId) < 0) {
                    user.listFriends.push(req.params.fromId);
                    user.save(function(err) {
                        if(err) return next(err);
                    })
                }
            });
            // add friend from
            models.User.findOne({_id: req.params.fromId}, function(err, user) {
                if(err) return next(err);
                if(user.listFriends.indexOf(req.loggedUser._id) < 0) {
                    user.listFriends.push(req.loggedUser._id);
                    user.save(function(err) {
                        if(err) return next(err);
                        res.send({userFrom: user, loggedName: req.loggedUser.name});
                    })
                }
            });
        });
}

function ignoreFriendRequest(req, res, next) {
    models.Friend.remove({ $and: [{from: req.params.fromId}, {to: req.loggedUser._id}, {confirmed: false}]})
        .exec(function(err, friend) {
            if(err) return next(err);
            res.send({loggedName: req.loggedUser.name});
        });
}

function unfriend(req, res, next) {
    models.Friend.findOneAndRemove({ $or: [
        { $and : [{from: req.loggedUser._id}, {to: req.params.toId}] },
        { $and : [{from: req.params.toId}, {to: req.loggedUser._id}] }
    ]})
    .exec(function(err, doc) {
        if(err) return next(err);
        models.User.findOneAndUpdate({_id: req.loggedUser._id}, { $pull: {listFriends: req.params.toId} },
            function(err, user) {
                if(err) return next(err);
                models.User.findOneAndUpdate({_id: req.params.toId}, { $pull: {listFriends: req.loggedUser._id}},
                   function(err, user) {
                       if(err) return next(err);
                       res.send({status: true});
                   });
            });
    });
}
