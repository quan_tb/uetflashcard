'use strict';
const _ = require('lodash');

class Helper {
    constructor() {
        this.models = require('./models');
    }

    changeStatus(userId, status, callback) {
        this.models.User.findOneAndUpdate({_id: userId}, { $set: { 'online': status }}, {new: true},
        function(err, user){
           callback(err, user);
        });
    }

    unfriend(fromId, toId, callback) {
        this.models.User.findOneAndUpdate({_id: fromId}, { $pull: { listFriends: toId }}, (err, user) => {
                if(err) {
                    console.log('error occured');
                } else {
                    this.models.User.findOneAndUpdate({_id: toId}, { $pull: { listFriends: fromId }},
                        function (err, friend) {
                            callback(err, true);
                        });
                }
            });
    }

    generateContentGame(setId, callback) {
        var setAll = [], defAll = [];
        this.models.StudySet.findById(setId).exec(function (err, set) {
            if (err) {
                console.log(err);
            }
            if (!set) {
                console.log("Not found set");
            }
            _.forEach(set.sets, (value) => {
                defAll.push(value.definition);
            });
            _.forEach(set.sets, (value) => {
                var randomArray = _.sampleSize(_.filter(defAll, function(n) { return n !== value.definition; }), 3);
                randomArray.push(value.definition);
                var objectSet = {term: value.term, definition: value.definition, randomAnswer: _.shuffle(randomArray)};
                setAll.push(objectSet);
            });
            callback(err, _.sampleSize(setAll, 5));
        });
    }
}

module.exports = new Helper();