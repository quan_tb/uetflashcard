var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
var userTestSchema = new Schema({
    correct: Number,
    incorrect: Number,
    createdAt: {type: Date, default: Date.now},
    studySet: {type: ObjectId, ref: 'StudySet'}
})
var userSchema = new Schema({
    socialId: {type: String, required: true},
    name: {type: String, required: true},
    email: {type: String, required: true, email: true, unique: true},
    photo: {type: String, required: true},
    listFriends: [{type: String, ref: 'User'}],
    online: {type: Boolean, default: false},
    testResults: [userTestSchema],
    socketId: String,
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});
module.exports = userSchema;