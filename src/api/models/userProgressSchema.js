var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

// if a progress is updated from 10:00AM to 10:59AM and interval is 2 (hours) so it will repeat
// from 12:00PM to 12:59PM that means it is regardless minutes and seconds. that will make this app simple
var userProgressSchema = new Schema({
    studySet: {type: ObjectId, ref: 'StudySet'},
    user: {type: ObjectId, ref: 'User'},
    pair: {type: String, ref: 'StudySet.sets'},
    interval: {type: Number, default: 0}, // in hours
    ef: {type: Number, default: 2.5},
    n: {type: Number, default: 1},
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});

module.exports = userProgressSchema;
