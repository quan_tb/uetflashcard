var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);
var uniqueValidator = require('mongoose-unique-validator');
mongoose.Promise = require('bluebird');
// setup plugins
mongoose.plugin(uniqueValidator);
mongoose.plugin(deepPopulate);

module.exports.User = mongoose.model('User', require('./userSchema'));
module.exports.StudySet = mongoose.model('StudySet', require('./studySetSchema'));
module.exports.UserProgress = mongoose.model('UserProgress', require('./userProgressSchema'));
module.exports.Friend = mongoose.model('Friend', require('./friendSchema'));
module.exports.HistoryGame = mongoose.model('HistoryGame', require('./historyGameSchema'));
module.exports.Message = mongoose.model('Message', require('./messageSchema'));