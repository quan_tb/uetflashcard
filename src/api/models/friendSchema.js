var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var friendSchema = new Schema({
    from: {type: String, ref: 'User'},
    to: {type: String, ref: 'User'},
    confirmed: {type: Boolean, default: false},
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});

module.exports = friendSchema;