var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var historyGameSchema = new Schema({
    player1: {
        id: { type: String, ref:'User' },
        win: { type: Number, default: 0 }
    },
    player2: {
        id: { type: String, ref:'User' },
        win: { type: Number, default: 0 }
    },
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}

});

module.exports = historyGameSchema;