var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var messageSchema = new Schema({
    userId: {type: String, ref:'User'},
    message: {type: String},
    createdAt: {type: Date, default: Date.now}
});

module.exports = messageSchema;