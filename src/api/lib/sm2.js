module.exports = SM2;
function SM2(eFactor, qualityResponse) {
    this.eFactor = eFactor;
    this.qualityResponse = qualityResponse;
    var defaultEFactor = 2.5;
    if (qualityResponse < 3) {
        this.eFactor = defaultEFactor;
    }
}

SM2.prototype.getNextInterval = function (n) {
    if (n == 1) {
        return 1
    }
    else if (n == 2) {
        return 6
    }
    else if (n > 2) {
        return (n - 1) * this.eFactor;
    }
    else {
        return 0
    }
};

SM2.prototype.getNewEFactor = function () {
    var newEFactor = this.eFactor + (0.1 - (5 - this.qualityResponse) * (0.08 + (5 - this.qualityResponse) * 0.02));

    if (newEFactor < 1.3) {
        newEFactor = 1.3
    }
    return newEFactor
};