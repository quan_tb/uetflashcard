import React, {Component} from 'react';
import request from 'superagent';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import {browserHistory} from 'react-router';
import routes from './routes';
import {Grid, Row, Col} from 'react-flexbox-grid';
import VoicePlayer from './VoicePlayer';
import {ShareButtons, ShareCounts, generateShareIcon} from 'react-share';
const FileSaver = require('file-saver');
const _ = require('lodash');

const style = {
    margin: 12,
};
const { FacebookShareButton, GooglePlusShareButton } = ShareButtons;
const FacebookIcon = generateShareIcon('facebook');
const GooglePlusIcon = generateShareIcon('google');

export default class extends Component{
    constructor(){
        super();
        this.state = {
            setDetail: {},
            studySet: [],
            currentTerm: '',
            play: false,
            loading: true
        }
    }
    componentDidMount() {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        if (this.props.params.setId) {
            request
                .get('/api/v1/sets/' + this.props.params.setId)
                .set('QUIZ-TOKEN', quizToken)
                .end(function (err, res) {
                    if (err) {
                    } else {
                        var doc = res.body.data;
                        that.setState({
                            studySet: doc.sets,
                            setDetail: {
                                title: doc.title,
                                description: doc.description,
                                image: doc.image,
                                public: doc.public,
                                termLanguage: doc.termLanguage,
                                id: doc._id
                            },
                            loading: false
                        });
                    }
                });
        }
    }
    onPlay(term){
        this.setState({
            currentTerm: term,
            play: true
        })
    }
    onEnd(){
        this.setState({
            currentTerm: '',
            play: false
        })
    }
    onStartLearn(){
        let that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        request
            .get('/api/v1/sets/' + this.props.params.setId + '/learn')
            .set('QUIZ-TOKEN', quizToken)
            .end(function (err, res) {
                if (err) {
                } else {
                    if (res.body.status){
                        browserHistory.push(routes.ROUTES.VIEW_SET_BASE + that.props.params.setId + '/learn')
                    }
                }
            });
    }

    exportText() {
        let dataArray = [];
        this.state.studySet.forEach((value) => {
           let objectValue = _.join(_.map(_.omit(value, ['id']), function(e) {
               return e;
           }),',');
           dataArray.push(objectValue);
        });
        const blob = new Blob([dataArray.join('\n')], {type: "text/plain;charset=utf-8"});
        FileSaver.saveAs(blob, this.state.setDetail.title + ".txt");
    }

    render(){
        var that = this;
        const shareUrl = "http://uetflashcard.me/studySet/" + that.state.setDetail.id;
        const title = "Uet flashcard";
        const list = this.state.studySet.map((item) =>
            (
                <Row key={item.id}>
                    <Col sm={4} style={{paddingTop: 10}}>{item.term}</Col>
                    <Col sm={4} style={{paddingTop: 10}}>{item.definition}</Col>
                    <Col sm={4}>
                        <RaisedButton label="Play" primary={true} style={style} onTouchTap={that.onPlay.bind(that, item.term)} />
                    </Col>
                    <Col sm={12}><hr /></Col>
                </Row>

            )
        );
        return (
            <div>
                {this.state.play && (
                    <VoicePlayer play text={this.state.currentTerm} lang={this.state.setDetail.termLanguage} onEnd={this.onEnd.bind(this)} />
                )}
                {(this.state.loading) ?
                <div className="preloader-wrapper big active uet-loading">
                    <div className="spinner-layer spinner-blue-only">
                        <div className="circle-clipper left">
                            <div className="circle"></div>
                        </div>
                        <div className="gap-patch">
                            <div className="circle"></div>
                        </div>
                        <div className="circle-clipper right">
                            <div className="circle"></div>
                        </div>
                    </div>
                </div> :
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <h3 className="custom-share" style={{color : '#1e88e5'}}>{this.state.setDetail.title}</h3>
                            <FacebookShareButton url={shareUrl} title={title} className="custom-share">
                                <FacebookIcon size={32} round />
                            </FacebookShareButton>
                            <GooglePlusShareButton url={shareUrl} title={title} className="custom-share">
                                <GooglePlusIcon size={32} round/>
                            </GooglePlusShareButton>
                            <i className="material-icons custom-share" onClick={this.exportText.bind(this)}>cloud_download</i>
                            <div><img style={{float: 'left', margin: '0 15px 15px 0', width: 300}} src={this.state.setDetail.image? this.state.setDetail.image : '/photo_holder.png'} alt={this.state.setDetail.title}/>{this.state.setDetail.description}</div>
                            <div style={{clear: 'both'}}>
                                <RaisedButton label="Flashcard" primary={true} style={style} onTouchTap={()=>{browserHistory.push(routes.ROUTES.VIEW_SET_BASE + this.state.setDetail.id + '/flashcard')}} />
                                <RaisedButton label="Learn" primary={true} style={style} onTouchTap={this.onStartLearn.bind(this)} />
                                <RaisedButton label="Test" primary={true} style={style} onTouchTap={()=>{browserHistory.push(routes.ROUTES.VIEW_SET_BASE + this.state.setDetail.id + '/test')}} />
                                <RaisedButton label="Game" secondary={true} style={style} onTouchTap={()=> { browserHistory.push(routes.ROUTES.VIEW_SET_BASE + this.state.setDetail.id + '/game') }}/>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12}>
                            <h4>List terms/definitions</h4>
                            <div>
                                {list}
                            </div>
                        </Col>
                    </Row>
                </Grid>
                }
            </div>
        )
    }
}