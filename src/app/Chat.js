import React, {Component} from 'react';
import Avatar from 'material-ui/Avatar';
import NiceTextField from './components/NiceTextField';
import moment from 'moment';
import request from 'superagent';
import _ from 'lodash';
import {browserHistory} from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import routes from './routes';

export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedUser: [],
            messageList: []
        };
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    componentDidMount() {
        var that = this;
        if(this.props.loggedUser) {
            this.setState({loggedUser: this.props.loggedUser});
        }
        this.getMessageList();
        window.socketVar.on('receive chat', () => {
           this.getMessageList();
        });
    }

    getMessageList() {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        request
            .get('api/v1/message')
            .set('QUIZ-TOKEN', quizToken)
            .end(function(err, res) {
                if(err) {
                    Materialize.toast(err, 5000);
                } else {
                    var result = res.body.data.reduce(function(prev, curr) {
                        if (prev.length && curr.userId._id === prev[prev.length - 1][0].userId._id) {
                            prev[prev.length - 1].push(curr);
                        }
                        else {
                            prev.push([curr]);
                        }
                        return prev;
                    }, []).map((g) => _.mergeWith(...g, (o, s, k) => k === 'message' ? o + '\n' + s : o));
                    that.setState({
                        messageList: result
                    }, () => {
                        var objDiv = document.getElementById("content-chat");
                        objDiv.scrollTop = objDiv.scrollHeight;
                    });
                }
            })
    }

    componentWillReceiveProps(props) {
        this.setState({loggedUser: props.loggedUser});
    }

    onSubmitMessage() {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        request
            .post('/api/v1/message')
            .set('QUIZ-TOKEN', quizToken)
            .send({message: that.message.getValue()})
            .end(function(err, res) {
                if(err) {
                    Materialize.toast(err, 5000);
                } else {
                    if(res.body.status) {
                        that.message.setValue('');
                        window.socketVar.emit('send chat');
                        that.getMessageList();
                    }
                }
            })
    }

    handleKeyPress(event) {
        var that = this;
        if(event.key == 'Enter'){
            that.onSubmitMessage();
        }
    }

    render() {

        var messageList = this.state.messageList.map((item) => {
            var message = item.message.split('\n').map((item, key) =>
                <span key={key}>{item}<br/></span>
            );
            return (
            <div className="row" key={item._id} style={{marginBottom: '0px'}}>
                <div className="col s1">
                    <Avatar size={30} style={{marginTop: '10px', marginLeft: '50%'}} src={item.userId.photo}/>
                </div>
                <div className="col s9">
                    <p><b>{item.userId.name}</b></p>
                    <p>{message}</p>
                </div>
                <div className="col s2">
                    <p>{moment(item.createdAt).format('DD/MM/YYYY, hh:mm:ss a')}</p>
                </div>
            </div>
            );
        });
        return (
            <div className="container">
                <div className="row">
                    <div className="col s8 offset-s2">
                        <div className="card">
                            <div className="card-content" >
                                <span className="card-title blue-text">Chat Room</span>
                                <div id="content-chat" style={{maxHeight: '500px', overflowY: 'auto', overflowX: 'hidden'}}>
                                {
                                    (this.state.messageList.length == 0) ?
                                        <div className="row center">No message to show</div> : messageList
                                }
                                </div>
                            </div>
                            <div className="card-action" style={{paddingTop: '0px'}}>
                                <div className="col s1">
                                    <Avatar size={30} style={{marginTop: '20px'}} src={this.state.loggedUser.photo}/>
                                </div>
                                <div className="col s10">
                                    <NiceTextField ref={(input) => { this.message = input; }} floatingLabelText="Enter message" fullWidth={true} type="text" hintText="Enter message to chat" onKeyPress={this.handleKeyPress}/><br />
                                </div>
                                <div className="col s1">
                                    <i className="material-icons" onClick={this.onSubmitMessage.bind(this)} style={{color: '#1e88e5', marginTop: '23px', cursor: 'pointer'}}>send</i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col s2">
                        <RaisedButton style={{margin: '10px 0 0 10px'}} secondary={true} label="Back"
                                      onTouchTap={() => browserHistory.push(routes.ROUTES.INDEX)}/>
                    </div>
                </div>
            </div>
        );
    }
}
