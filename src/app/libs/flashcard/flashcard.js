(function ($) {
    $.fn.flashcard = function (options) {
        var container = $(this);
        container.find("#userResponse").hide();
        if (options == 'destroy'){
            return container.find('#deck').cycle('destroy');
        }
        $(options.flipper).off('click').on('click', function(e){
            e.preventDefault();
            container.find("#userResponse").slideToggle();
            container.find('.cardf.current').toggleClass('flip');
        });
        // CYCLE
        if (container.find('#deck').children().length > 1){
            container.find('#deck').cycle({
                after:   onAfter,
                before:  onBefore,
                fx:      'shuffle',
                next:    options.next,
                prev:    options.prev,
                shuffle: {
                    top:   -300,
                    left:  20
                },
                speed:   'fast',
                timeout: 0,
                nowrap: 1,
                autostop: 1
            });
        } else {
            container.find('#deck').children().first().show();
        }
        function onBefore(prev, cur){
            $(this).parent().find('.current').removeClass('current');
            if ($(cur).index() == 0 && options.prev){
                $(options.prev).prop('disabled', true);
            }
            if ($(cur).index() < container.find('#deck').children().length -1 && options.next){
                $(options.next).prop('disabled', false);
            }
            $(options.progress).find('span').text(($(cur).index() + 1) + '/' + container.find('#deck').children().length);
        }
        function onAfter(prev, cur){
            $(this).addClass('current');
            if ($(cur).index() == container.find('#deck').children().length -1 && options.next){
                $(options.next).prop('disabled', true);
            }
            if ($(cur).index() > 0 && options.prev){
                $(options.prev).prop('disabled', false);
            }
            $(options.progress).find('span').text(($(cur).index() + 1) + '/' + container.find('#deck').children().length);
        }
        // Keyboard Nav
        $(document).keydown(function (e) {
            var keyCode = e.keyCode || e.which;
            var key = {left: 37, up: 38, right: 39, down: 40, enter: 13, space: 32, questionMark: 191 };
            switch (keyCode) {
                case key.left:
                    container.find('#deck').cycle('prev');
                    e.preventDefault();
                    break;
                case key.right:
                    container.find('#deck').cycle('next');
                    e.preventDefault();
                    break;
                case key.up:
                case key.down:
                case key.enter:
                case key.space:
                    container.find('.current').toggleClass('flip');
                    e.preventDefault();
                    break;
                case key.questionMark:
                    container.find('#keyboard_shortcuts').fadeToggle();
                    e.preventDefault();
                    break;
            }
        });
        container.find('#keyboard_shortcuts_toggle').click(function(){
            container.find('#keyboard_shortcuts').fadeToggle();
        });
    }
})(jQuery);