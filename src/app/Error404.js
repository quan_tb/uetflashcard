import React, {Component} from 'react';
class Error404 extends Component {
    render(){
        return <div style={{paddingTop: 100}}>
            <h1 style={{textAlign: 'center'}}>404 Not Found</h1>
        </div>
    }
}
export default Error404;