import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {browserHistory} from 'react-router';
import routes from './routes';
import $ from 'jquery';
import './libs/flashcard/jquery.circle';
import './libs/flashcard/flashcard';
import request from 'superagent';
import RaisedButton from 'material-ui/RaisedButton';

export default class extends Component {
    constructor(){
        super();
        this.state = {
            studySet: []
        }
    }
    componentDidMount() {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        if (this.props.params.setId) {
            request
                .get('/api/v1/sets/' + this.props.params.setId)
                .set('QUIZ-TOKEN', quizToken)
                .end(function (err, res) {
                    if (err) {
                    } else {
                        var doc = res.body.data;
                        that.setState({
                            studySet: doc.sets,
                            setId: doc._id
                        });
                        $(that.container).flashcard({
                            next: ReactDOM.findDOMNode(that.nextBtn),
                            prev: ReactDOM.findDOMNode(that.prevBtn),
                            flipper: ReactDOM.findDOMNode(that.flipperBtn),
                            progress: ReactDOM.findDOMNode(that.progressBtn)
                        });
                    }
                });
        }
    }
    componentWillUnmount() {
    }
    render() {
        const items = this.state.studySet.map((item) =>
            <li key={item.id} className="cardf">
                <div className="side_one">
                    <p>{item.term}</p>
                </div>
                <div className="side_two">
                    <p>{item.definition}</p>
                </div>
            </li>
        );
        return <div ref={(d) => this.container = d} className={this.props.className}>
            <ul id="deck">
                {items}
            </ul>
            <div id="nav_deck">
                <RaisedButton label="N/A" ref={(e) => this.progressBtn = e} />
                <RaisedButton style={{marginLeft: 10}} primary={true} label="Prev" ref={(e) => this.prevBtn = e} />
                <RaisedButton style={{marginLeft: 10}} primary={true} label="Show Answer" ref={(e) => this.flipperBtn = e} />
                <RaisedButton style={{marginLeft: 10}} primary={true} label="Next" ref={(e) => this.nextBtn = e} />
                <RaisedButton style={{marginLeft: 10}} secondary={true} label="Done" onTouchTap={() => browserHistory.push(routes.ROUTES.VIEW_SET_BASE + this.state.setId)} />
            </div>
        </div>;
    }
}