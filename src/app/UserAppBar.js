import React, {Component} from "react";
import PropTypes from 'prop-types';
import IconMenu from "material-ui/IconMenu";
import MenuItem from "material-ui/MenuItem";
import FlatButton from "material-ui/FlatButton";
import Avatar from "material-ui/Avatar";
import Snackbar from "material-ui/Snackbar";
import Dialog from "material-ui/Dialog";
import ExpandMore from "material-ui/svg-icons/navigation/expand-more";
import FacebookLogin from "react-facebook-login";
import GoogleLogin from "react-google-login";
import {Link, browserHistory} from "react-router";
import request from "superagent";
import config from "./config";
import routes from "./routes";

class Login extends Component {
    static muiName = 'FlatButton';
    static propTypes = {
        successLogin: PropTypes.func.isRequired,
    };
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    constructor(props, context) {
        super(props, context);

        this.state = {
            openLoginModal: false,
            openErrorLogin: false
        };
    }

    openLoginModal(){
        this.setState({openLoginModal: true});
    }

    hideModal(){
        this.setState({openLoginModal: false});
    }

    responseFacebook = (response) => {
        var that = this;
        request
            .post('/api/v1/auth')
            .send(response)
            .end(function(err, res){
                if (err) {
                    that.setState({openErrorLogin: true});
                } else {
                    window.localStorage.setItem('quizToken', res.body.data.quizToken);
                    that.props.successLogin(res.body.data.loggedUser);
                }
            });
    };

    responseGoogle = (response) => {
        console.log(response);
        var that = this;
        var profile = response.profileObj;
        request
            .post('/api/v1/auth')
            .send(profile)
            .end(function(err, res){
                if (err) {
                    that.setState({openErrorLogin: true});
                } else {
                    window.localStorage.setItem('quizToken', res.body.data.quizToken);
                    that.props.successLogin(res.body.data.loggedUser);
                }
            });
    };

    responseGoogleFailure = (response) => {
        Materialize.toast(response, 5000);
    };

    render() {
        const customContentStyle = {
            width: '40%',
            maxWidth: 'none',
            padding : '20px',
            textAlign: 'center'
        };

        return (
            <div>
                <a href="#" className="waves-effect waves-light btn red darken-1" style={{float: 'right', marginTop: '12px',marginLeft: '20px'}} onClick={this.openLoginModal.bind(this)}>
                    Login
                </a>
                <ul className="right hide-on-med-and-down">
                    <li><a href="#intro">Feature</a></li>
                    <li><a href="#team">Team</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
                <ul id="nav-mobile" className="side-nav">
                    <li><a href="#intro">Feature</a></li>
                    <li><a href="#team">Team</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
                <a href="#" data-activates="nav-mobile" className="button-collapse"><i className="mdi-navigation-menu" /></a>
                <Snackbar
                    open={this.state.openErrorLogin}
                    message="Could not log in. Please try again"
                    autoHideDuration={4000}
                />
                <Dialog
                    open={this.state.openLoginModal}
                    contentStyle={customContentStyle}
                    onRequestClose={this.hideModal.bind(this)}
                >
                    <FacebookLogin
                        appId={config.facebookAppId}
                        autoLoad={false}
                        fields="name,email,picture"
                        callback={this.responseFacebook.bind(this)}
                    />
                    <GoogleLogin
                        clientId={config.googleClientId}
                        tag="div"
                        className="googleLoginButton"
                        onSuccess={this.responseGoogle.bind(this)}
                        onFailure={this.responseGoogleFailure.bind(this)}
                    />
                </Dialog>
            </div>
        );
    }
}

var that = this;
const Logged = (props) => {
    var listRequest = props.friendRequest.map((item) =>
        <li key={item._id} className="collection-item avatar">
            <img src={item.photo} alt="" className="circle" />
            <span className="title">{item.name}</span>
            <p>{item.email}</p>
            <a href="#" onClick={props.acceptFriendRequest.bind(that, item._id)} className="secondary-content" style={{top: "10px", right: "30px"}} ><i className="material-icons">done</i>
            </a>
            <a href="#" onClick={props.ignoreFriendRequest.bind(that, item._id)} className="third-content"><i id="delete" className="material-icons">delete</i>
            </a>
        </li>
    );
    return (
    <div>
        <div style={{float: 'right'}}>
            <IconMenu
                iconButtonElement={
                    <FlatButton style={{color: "#fff"}} labelPosition="before" label={props.loggedUser.name} icon={<ExpandMore />} />
                }
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
            >
                <MenuItem primaryText="My Profile" onTouchTap={()=>{browserHistory.push(routes.ROUTES.USER_PROFILE_BASE + props.loggedUser._id)}} />
                <MenuItem primaryText="" ><Link style={{textDecoration: 'none', color: '#000', display: 'block'}} to={routes.ROUTES.MY_LEARNING}>My Study Sets</Link></MenuItem>
                <MenuItem primaryText="Sign out" onTouchTap={props.doLogout}/>
            </IconMenu>
        </div>
        <div style={{float: 'right'}}>
            <Avatar style={{marginTop: '15px'}}  size={30} src={props.loggedUser.photo} />
        </div>
        <div style={{float: 'right'}}>
            <Link to={routes.ROUTES.CHAT}>
                <i className='material-icons' style={{paddingRight: '15px', marginTop: '20px', cursor: 'pointer'}}>chat</i>
            </Link>
        </div>
        <div style={{float: 'right', position: 'relative'}} >
           <i className='dropdown-button material-icons' href='#' data-activates='notification-friend' style={{paddingRight: '15px', marginTop: '20px', cursor: 'pointer'}}>supervisor_account</i>
           { (props.friendRequest.length !== 0) &&
                <span id="notification-count">{props.friendRequest.length}</span>

           }
            <ul id='notification-friend' className='dropdown-content collection' style={{marginTop: '20px'}}>
                {
                    (props.friendRequest.length !== 0) ? listRequest :
                        <li className="collection-item">No requests to show.</li>
                }
            </ul>
        </div>
        <Link to={routes.ROUTES.CREATE} className="waves-effect waves-light btn red darken-1" style={{float: 'right', marginTop: '12px',marginRight: '20px'}}>
            Create
        </Link>
    </div>
    )
};

Logged.muiName = 'IconMenu';

/**
 * This example is taking advantage of the composability of the `AppBar`
 * to render different components depending on the application state.
 */
class UserAppBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            logged: false,
            loggedUser: null,
            friendRequest: [],
            open: false,
            player: {},
            countDownTime : config.timeOutGameRequest,
            setGameId: ''
        };
        this.updateName = this.updateName.bind(this);
        this.timer = this.timer.bind(this);
    }

    componentDidMount(){
        var that = this;
        var quizToken = window.localStorage.getItem('quizToken');
        if (!quizToken){
            that.setState({logged: false});
            return;
        }
        request
            .get('/api/v1/')
            .set('QUIZ-TOKEN', quizToken)
            .end(function(err, res){
                if (err) {
                    that.setState({logged: false});
                } else {
                    if (res.status) {
                        that.setState({logged: true, loggedUser: res.body.data});
                        that.forSocket(res.body.data);
                    }
                }
            });
        that.getListRequest();
    }

    forSocket(data) {
        var that = this;
        window.socketVar.emit('new user', {userId: data._id});
        window.socketVar.on('send-friend-request', (data) => {
            Materialize.toast(data['fromFriend'] + " want to add you into friend list", 5000);
            that.getListRequest();
        });
        window.socketVar.on('response friend', (data) => {
            Materialize.toast(data['fromFriend'] + " and you becomes friend now", 5000);
        });
        window.socketVar.on('response ignore', (data) => {
            Materialize.toast(data['fromFriend'] + " ignored your friend request!", 5000);
        });
        window.socketVar.on('noti-unfriend', (data) => {
            Materialize.toast(data['unfriendName'] + " unfriend you !", 5000)
        });
        window.socketVar.on('send game request', (data) => {
            var interval = setInterval(this.timer,1000);
            that.setState({
                open: true,
                player: data['fromUser'],
                setGameId: data['setId'],
                intervalId: interval
            });

        });
    }

    getListRequest() {
        var that = this;
        var quizToken = window.localStorage.getItem('quizToken');
        request
            .get('/api/v1/request')
            .set('QUIZ-TOKEN', quizToken)
            .end(function(err, res) {
                if(err) {
                } else {
                    that.setState({
                        friendRequest: res.body
                    });
                }
            })
    }

    onSuccessLogin(data){
        this.setState({logged: true, loggedUser: data});
        this.getListRequest();
        this.forSocket(data);
        browserHistory.replace(routes.ROUTES.MY_LEARNING)
    }

    doLogout(){
        window.localStorage.setItem('quizToken', '');
        this.setState({logged: false});
        browserHistory.replace(routes.ROUTES.INDEX);
        window.location.reload();
    }

    updateName() {
        var that = this;
        var quizToken = window.localStorage.getItem('quizToken');
        request
            .get('/api/v1/')
            .set('QUIZ-TOKEN', quizToken)
            .end(function(err, res){
                if (err) {
                    that.setState({logged: false});
                } else {
                    if (res.status){
                        that.setState({logged: true, loggedUser: res.body.data});
                    }
                }
            });
    }

    acceptFriendRequest(id) {
        var quizToken = window.localStorage.getItem('quizToken');
        var that = this;
        request
            .post('/api/v1/request/accept')
            .set('QUIZ-TOKEN', quizToken)
            .send({fromId: id})
            .end(function(err, res) {
               if(err) {
                   Materialize.toast(err, 5000);
               } else {
                    window.socketVar.emit('accept request', {userFromId: id, userToId: that.state.loggedUser._id, userTo: res.body.loggedName, userFrom: res.body.userFrom.name});
                    that.getListRequest();
               }
            });
    }

    ignoreFriendRequest(id) {
        var quizToken = window.localStorage.getItem('quizToken');
        var that = this;
        request
            .post('/api/v1/request/ignore')
            .set('QUIZ-TOKEN', quizToken)
            .send({fromId: id})
            .end(function(err, res) {
                if(err) {
                    Materialize.toast(err, 5000);
                } else {
                    Materialize.toast('Ignored request successfully!', 5000);
                    window.socketVar.emit('ignore request', {userFromId: id, userTo: res.body.loggedName});
                    that.getListRequest();
                }
            })
    }

    timer = () => {
        this.setState({
            countDownTime: this.state.countDownTime - 1
        });
        if(this.state.countDownTime <= 0) {
            clearInterval(this.state.intervalId);
            this.setState({
                open: false,
                countDownTime: config.timeOutGameRequest
            });
            Materialize.toast("You don't response for game request", 5000);
        }
    };

    handleClose = () => {
        this.setState({open: false});
        clearInterval(this.state.intervalId);
        Materialize.toast('You have ignore a game request', 5000);
        window.socketVar.emit('ignore game request', {userFrom: this.state.player});
    };

    acceptGameRequest = () => {
        this.setState({open: false});
        clearInterval(this.state.intervalId);
        window.socketVar.emit('accept game request', {loggedUser: this.state.loggedUser, toUser: this.state.player, setId: this.state.setGameId});
        browserHistory.push(routes.ROUTES.VIEW_SET_BASE + this.state.setGameId + '/game');
    };

    render() {
        var that = this;
        var children = React.Children.map(this.props.children, function(child) {
            return React.cloneElement(child, {
                logged: that.state.logged,
                loggedUser: that.state.loggedUser,
                updateName: that.updateName
            });
        });
        const actions = [
            <FlatButton
                label="Confirm"
                primary={true}
                onTouchTap={this.acceptGameRequest}
            />,
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose}
            />,
        ];
        return (

            <div>
                <div className="navbar-fixed">
                    <nav id="nav_f" className="default_color" role="navigation">
                        <div className="container">
                            <Link to={routes.ROUTES.INDEX} id="logo-container" className="brand-logo">Uet Flashcard</Link>
                            {
                                this.state.logged ? <Logged loggedUser={this.state.loggedUser} friendRequest={this.state.friendRequest}  doLogout={this.doLogout.bind(this)} acceptFriendRequest={this.acceptFriendRequest.bind(this)} ignoreFriendRequest={this.ignoreFriendRequest.bind(this)} />
                                    : <Login successLogin={this.onSuccessLogin.bind(this)} />
                            }
                            <Dialog
                                title="Confirm"
                                modal={false}
                                actions={actions}
                                open={this.state.open}
                                onRequestClose={this.handleClose}
                            >
                                <div className="row" style={{marginTop: '30px'}}>
                                    <div className="col s5">
                                        <div className="card-panel grey lighten-5 z-depth-1">
                                            <div className="row" style={{marginBottom: '0px'}}>
                                                <div className="col s4">
                                                    { this.state.loggedUser &&
                                                    <img src={this.state.loggedUser.photo} alt="" className="circle responsive-img" />
                                                    }
                                                </div>
                                                <div className="col s8">
                                                    { this.state.loggedUser &&
                                                    <p className="black-text">
                                                        {this.state.loggedUser.name}
                                                    </p>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col s2 center">
                                        <p className="circle blue white-text" style={{display: 'inline-block', padding: '20px'}}>VS.</p>
                                    </div>
                                    <div className="col s5">
                                        <div className="card-panel grey lighten-5 z-depth-1">
                                            <div className="row" style={{marginBottom: '0px'}}>
                                                <div className="col s4">
                                                    <img src={this.state.player.photo} alt="" className="circle responsive-img" />
                                                </div>
                                                <div className="col s8">
                                                    <p className="black-text">
                                                        {this.state.player.name}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Dialog>
                        </div>
                    </nav>
                </div>
                <div className="content">{children}</div>
            </div>
        );
    }
}

export default UserAppBar;