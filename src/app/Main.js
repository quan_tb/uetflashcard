/**
 * In this file, we create a React component
 * which incorporates components provided by Material-UI.
 */
import React, {Component} from 'react';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {browserHistory, Router} from 'react-router';
import UserAppBar from './UserAppBar';
import Home from './Home';
import StudyForm from './StudyForm';
import FlashCards from './FlashCards';
import TestFlashCards from './TestFlashCards';
import ViewStudySet from './ViewStudySet';
import LearnSuperMemo from './LearnSuperMemo';
import GameFlashCard from './GameFlashCard';
import MyLearning from './MyLearning';
import UserProfile from './UserProfile';
import Chat from './Chat';
import Footer from './Footer';
import Error404 from './Error404';
import routes from './routes';
import { createStore } from 'redux';
import actions from './reducers';
import { Provider } from 'react-redux';
import './libs/materialize';
import './libs/custom-min';
import io from 'socket.io-client';

require("./libs/css/plugin-min.css");
require("./libs/css/custom-min.css");
require("./libs/css/main.css");

const env = process.env.NODE_ENV || 'development';
const config = require('../config/' + env);
const store = createStore(actions);
const muiTheme = getMuiTheme();
const reactRoutes = {
    path: '/',
    component: UserAppBar,
    indexRoute: {component: Home},
    childRoutes: [
        { path: routes.ROUTES.CREATE, component: StudyForm },
        { path: routes.ROUTES.VIEW_SET_DETAIL, component: ViewStudySet},
        { path: routes.ROUTES.FLASHCARD, component:  FlashCards},
        { path: routes.ROUTES.TEST_FLASHCARD, component: TestFlashCards},
        { path: routes.ROUTES.LEARN, component: LearnSuperMemo},
        { path: routes.ROUTES.GAME, component: GameFlashCard},
        { path: routes.ROUTES.USER_PROFILE_DETAIL, component: UserProfile},
        { path: routes.ROUTES.EDIT_SET_PARAMS, component: StudyForm },
        { path: routes.ROUTES.MY_LEARNING, component: MyLearning },
        { path: routes.ROUTES.CHAT, component: Chat},

        { path: '*', component: Error404}
    ]
};

class Main extends Component {
    componentWillMount() {
        window.socketVar = io(config.urlSocket);
    }
    render() {
        return (
            <Provider store={store}>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <div>
                        <div className="calc-content">
                            <Router history={browserHistory} routes={reactRoutes}>
                            </Router>
                        </div>
                        <Footer />
                    </div>
                </MuiThemeProvider>
            </Provider>

        );
    }
}
export default Main;
