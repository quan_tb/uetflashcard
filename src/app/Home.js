import React, {Component} from 'react';
import StudySets from './StudySets';
import LandingPage from './LandingPage';

class Home extends Component {

    render(){
        return <div>
            { this.props.logged ? <StudySets homepage={true} url='/api/v1/sets/' /> : <LandingPage /> }
        </div>;
    }
}

export default Home;