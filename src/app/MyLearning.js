import React, {Component} from 'react';
import {Grid, Row, Col} from 'react-flexbox-grid';
import StudySets from './StudySets';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import NiceTextField from './components/NiceTextField';
import request from 'superagent';

const customContentStyle = {
    width: '95%',
    maxWidth: 'none',
};

class MyLearning extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            searchedSet: [],
            updateSet : false
        };
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    onSubmitTextSearch = () => {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        request
            .post('/api/v1/search')
            .set('QUIZ-TOKEN', quizToken)
            .send({textSearch: that.searchText.getValue()})
            .end((err, res) => {
                if(err) {
                    Materialize.toast(err, 5000);
                } else {
                    that.setState({searchedSet: res.body.data});
                    that.searchText.setValue('');
                }
            });
    };

    saveSet = (id) => {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        request
            .post('/api/v1/otherSets')
            .set('QUIZ-TOKEN', quizToken)
            .send({setId: id})
            .end((err, res) => {
                if(err) {
                    Materialize.toast(err, 5000);
                } else {
                    if(res.body.status) {
                        Materialize.toast('Add set successfully!', 5000);
                        that.setState({updateSet: true});
                    }
                }
            });
    };

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleKeyPress = (event) => {
        var that = this;
        if(event.key == 'Enter'){
            that.onSubmitTextSearch();
        }
    };

    render(){
        const listSet = this.state.searchedSet.map((item, key) =>
            <tr key={item.id}>
                <td>{key + 1}</td>
                <td><a href={'https://quizlet.com' + item.url}>{item.title}</a></td>
                <td>{item.created_by}</td>
                <td>{item.term_count}</td>
                <td>{item.lang_terms}</td>
                <td>{item.lang_definitions}</td>
                <td>
                    <RaisedButton label="Add study set" onTouchTap={this.saveSet.bind(this, item.id)}/>
                </td>
            </tr>
        );
        return <div>
            <Grid>
                <Row>
                    <Col sm={12}>
                        <h5 style={{display: 'inline-block', marginRight: '20px'}}>My Study Sets</h5>
                        <RaisedButton label="Import from other sites" onTouchTap={this.handleOpen}/>
                        <Dialog
                            title="Import study set from other sites"
                            modal={false}
                            contentStyle={customContentStyle}
                            open={this.state.open}
                            onRequestClose={this.handleClose}
                        >
                            <NiceTextField ref={(input) => { this.searchText = input; }} floatingLabelText="Enter text to search set" fullWidth={true} type="text" hintText="Enter text to search" onKeyPress={this.handleKeyPress}/><br />
                            {this.state.searchedSet == 0 ?
                                <p className="center">List study sets is empty.</p> :
                                <div className="modal-set">
                                    <table>
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Title</th>
                                            <th>Created By</th>
                                            <th>Term count</th>
                                            <th>Lang term</th>
                                            <th>Lang definition</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {listSet}
                                        </tbody>
                                    </table>
                                </div>
                            }
                        </Dialog>
                    </Col>
                </Row>
            </Grid>
            
            <StudySets url="/api/v1/sets/byUser" updateSet={this.state.updateSet}/>
        </div>;
    }
}

export default MyLearning;