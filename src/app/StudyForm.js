import React, {Component} from 'react';
import NiceTextField from './components/NiceTextField';
import SelectField from 'material-ui/SelectField';
import Toggle from 'material-ui/Toggle';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import MenuItem from 'material-ui/MenuItem';
import Snackbar  from 'material-ui/Snackbar';
import IconDelete from 'material-ui/svg-icons/action/delete';
import shortid from 'shortid';
import {Grid, Row, Col} from 'react-flexbox-grid';
import request from 'superagent';
import {browserHistory} from 'react-router';
import Dropzone from 'react-dropzone';
import scrollToComponent from 'react-scroll-to-component';
import routes from './routes';

const PHOTO_HOLDER = '/photo_holder.png';

class NiceImage extends Component {
    constructor(props, context){
        super(props, context);
        this.state = {
            src: ''
        };
    }
    srcUpdate(url){
        this.setState({src: url})
    }
    render(){
        return <div style={{display: 'inline-block', float: 'left'}}><img style={{height: 200, maxWidth:'100%'}} src={this.state.src} alt="" /></div>
    }
}

class StudyForm extends Component {
    constructor(props, context){
        super(props, context);
        let studySet = [];
        for (let i = 0; i < 3; i++){
            studySet.push({id: shortid.generate(), term: '', definition: ''});
        }
        this.state = {
            studySet: studySet,
            showErrorMessage: false,
            studyImage: PHOTO_HOLDER,
            public: true,
            setId: ''
        };
        this.fieldsMap = {};
        this.handleChangePublic = this.handleChangePublic.bind(this);
    }

    handleAddMore = (event) => {
        var studySet = this.state.studySet;
        studySet.push({id: shortid.generate(), term: '', definition: ''});
        this.setState({
            studySet: studySet
        });
    };

    componentDidMount() {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        if (this.props.params.setId) {
            request
                .get('/api/v1/sets/' + this.props.params.setId)
                .set('QUIZ-TOKEN', quizToken)
                .end(function (err, res){
                    if (err) {
                    } else {
                        var doc = res.body.data;
                        that.setState({
                            studySet: doc.sets,
                            setId: doc._id,
                            termLanguage: doc.termLanguage,
                            definitionLanguage: doc.definitionLanguage,
                            public: doc.public,
                            studyImage: doc.image ? doc.image : PHOTO_HOLDER
                        });
                        that.thumbImage.srcUpdate(that.state.studyImage);
                        that.title.setValue(doc.title);
                        that.description.setValue(doc.description);
                        that.populateSets();
                    }
                });
        } else {
            this.thumbImage.srcUpdate(PHOTO_HOLDER);
        }
    }

    componentWillReceiveProps(nextProps){
        if (!nextProps.params.setId) {
            this.thumbImage.srcUpdate(PHOTO_HOLDER);
            this.title.setValue('');
            this.description.setValue('');
            var studySet = [];
            for (var i = 0; i < 3; i++){
                studySet.push({id: shortid.generate(), term: '', definition: ''});
            }
            this.setState({
                studySet: studySet,
                termLanguage: '',
                definitionLanguage: '',
                public: true,
                studyImage: PHOTO_HOLDER
            });
            this.populateSets();
        }
    }

    populateSets(){
        var that = this;
        this.state.studySet.forEach((item) => {
            that.fieldsMap[item.id].term.setValue(item.term);
            that.fieldsMap[item.id].definition.setValue(item.definition);
        });
    }

    handleDelete(id){
        if (confirm('Are you sure you want to delete this term/definition?')){
            var studySet = this.state.studySet;
            if(studySet.length > 3) {
                var newSet = studySet.filter((item) => {
                    return item.id != id;
                });
                this.setState({
                    studySet: newSet
                })
            } else {
                Materialize.toast("A study set must be least 3 terms and definitions", 5000);
            }
        }
    }

    handleFieldChange (event, newValue, item, prop){
        item[prop] = newValue;
    }

    handleChangePublic(event) {
        console.log(event.target.checked);
        this.setState({public: event.target.checked});
    }

    handleSave (){
        var quizToken = window.localStorage.getItem('quizToken');
        if (!quizToken){
            return;
        }

        if (this.validateForm()) {
            var data = {
                title: this.title.getValue(),
                description: this.description.getValue(),
                termLanguage: this.state.termLanguage,
                definitionLanguage: this.state.definitionLanguage,
                sets: this.state.studySet,
                image: this.state.studyImage,
                setId: this.state.setId,
                public: this.state.public
            };
            request
                .post('/api/v1/sets/' + this.state.setId)
                .set('QUIZ-TOKEN', quizToken)
                .send(data)
                .end(function(err, res){
                    if (err) {
                        Materialize.toast(err, 5000);
                    } else {
                        Materialize.toast('Saved study set successfully!', 5000);
                        browserHistory.replace(routes.ROUTES.MY_LEARNING);
                    }
                });
        }
    }

    validateForm(){
        var that = this;
        this.setState({submitted: true});
        this.title.setState({errorText: ''});
        this.description.setState({errorText: ''});
        var title = this.title.getValue();
        var description = this.description.getValue();
        var termLanguage = this.termLanguage.props.value;
        var definitionLanguage = this.definitionLanguage.props.value;
        if (!title){
            this.title.setState({errorText: 'Title could not be empty'});
            return false;
        }
        if (!description){
            this.description.setState({errorText: 'Description could not be empty'});
            return false;
        }
        if (!termLanguage || !definitionLanguage){
            return false;
        }
        var arrErr = [];
        Object.keys(this.fieldsMap).forEach((item) => {
            if (that.fieldsMap[item].term){
                if (!that.fieldsMap[item].term.getValue()){
                    that.fieldsMap[item].term.setState({errorText: 'Please enter term'});
                    arrErr.push(that.fieldsMap[item].term);
                } else {
                    that.fieldsMap[item].term.setState({errorText: ''})
                }
            }
            if (that.fieldsMap[item].definition){
                if (!that.fieldsMap[item].definition.getValue()){
                    that.fieldsMap[item].definition.setState({errorText: 'Please enter definition'})
                    arrErr.push(that.fieldsMap[item].term);
                } else {
                    that.fieldsMap[item].definition.setState({errorText: ''})
                }
            }

        });
        if (arrErr.length){
            setTimeout(() => scrollToComponent(arrErr[0]))
            return false;
        }
        return true;
    }

    onDrop(files){
        var quizToken = window.localStorage.getItem('quizToken');
        if (!quizToken){
            return;
        }
        var that = this;
        var formData = new FormData();
        files.forEach(function(file, index){
            formData.append('file'+index, file);
        });
        request
            .post('/api/v1/sets/upload')
            .set('QUIZ-TOKEN', quizToken)
            .send(formData)
            .end(function(err, res){
                if (err) {
                    that.setState({showErrorMessage: true});
                } else {
                    that.setState({studyImage: res.body.data.filename});
                    that.thumbImage.srcUpdate(res.body.data.filename);
                }
            });
    }

    onImportText(files) {
        var that = this;
        var formData = new FormData();
        let quizToken = window.localStorage.getItem('quizToken');
        files.forEach(function(file, index) {
           formData.append('file'+index, file);
        });
        request
            .post('/api/v1/sets/uploadText')
            .set('QUIZ-TOKEN', quizToken)
            .send(formData)
            .end(function(err, res) {
               if(err) {
                   Materialize.toast(err, 5000);
               } else {
                    if(res.body.data.length >= 3) {
                        that.setState({
                            studySet: res.body.data
                        });
                        that.populateSets();
                    } else {
                        Materialize.toast("A study set must be least 3 terms and definitions", 5000);
                    }
               }
            });
    }

    render(){
        const lists = this.state.studySet.map((item, index) =>
            <Row key={item.id}>
                <Col sm={4}>
                    <NiceTextField onChange={(event, newValue) => {this.handleFieldChange(event, newValue, item, 'term')}} ref={(input) => { this.fieldsMap[item.id] = {term: input}; }} floatingLabelText="Term" fullWidth={true} type="text" hintText="Example: Hello"/><br />
                </Col>
                <Col sm={1} />
                <Col sm={4}>
                    <NiceTextField onChange={(event, newValue) => {this.handleFieldChange(event, newValue, item, 'definition')}} ref={(input) => { this.fieldsMap[item.id].definition = input; }} floatingLabelText="Definition" fullWidth={true} type="text" hintText="Example: Xin chào"/><br />
                </Col>
                <Col sm={2}>
                    <FlatButton
                        icon={<IconDelete />}
                        onTouchTap={() => {this.handleDelete(item.id)}}
                        style={{marginTop: '20px'}}
                    />
                </Col>
            </Row>
        );
        return <div>
            <Grid>
                <Row>
                    <Col sm={8}>
                        <div>
                            <h5>
                                { this.props.params.setId ? 'Edit set' : 'Create a new study set' }
                            </h5>
                            <NiceTextField ref={(input) => { this.title = input; }} floatingLabelText="Enter title" fullWidth={true} type="text" hintText="Title"/><br />
                            <NiceTextField ref={(input) => { this.description = input; }} floatingLabelText="Enter description" fullWidth={true} type="text" hintText="Description"/><br />
                            <br/>
                            <div className="switch">
                                <label>
                                    Public
                                    <input type="checkbox" checked={this.state.public} onChange={this.handleChangePublic}/>
                                    <span className="lever"></span>
                                </label>
                            </div>
                            <br/>
                            <h5>Image</h5>
                            <Dropzone accept="image/*" onDrop={this.onDrop.bind(this)} style={{float: 'left', width: 200, height: 100, border: '1px dashed #eaeaea', marginRight: 10}}>
                                <div style={{textAlign: 'center', padding: '30px 5px', border: '1px dashed', cursor: 'pointer'}}>Try dropping some files here, or click to select files to upload.</div>
                            </Dropzone>
                            <NiceImage ref={(img) => this.thumbImage = img} src={this.state.studyImage} />

                        </div>
                    </Col>
                    <Col sm={4}>
                        <RaisedButton
                            label="Save"
                            onTouchTap={this.handleSave.bind(this)}
                            style={{marginTop: '20px', float: 'right'}}
                            primary={true}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col sm={5}>
                        <h5 style={{display: 'inline-block'}}>Terms</h5>
                        <Dropzone accept="text/plain" onDrop={this.onImportText.bind(this)} style={{width: 'auto', height: 'auto', border: '0px', display: 'inline-block', marginLeft: '15px', cursor: 'pointer'}}>
                            <i className="material-icons">backup</i>
                        </Dropzone>
                        <SelectField
                            errorText={!this.state.termLanguage && this.state.submitted? 'Term language could not be empty' : null}
                            floatingLabelText="Language"
                            value={this.state.termLanguage}
                            ref={(input) => this.termLanguage = input}
                            onChange={(event, index, value)=>{this.setState({termLanguage: value})}}
                            style={{display: 'block'}}
                        >
                            <MenuItem value={"en"} primaryText="English" />
                            <MenuItem value={"ja"} primaryText="Japanese" />
                            <MenuItem value={"vi"} primaryText="Vietnamese" />
                        </SelectField>
                        <br />
                    </Col>
                    <Col sm={7}>
                        <h5 style={{display: 'inline-block'}}>Definitions</h5>
                        <Dropzone accept="text/plain" onDrop={this.onImportText.bind(this)} style={{width: 'auto', height: 'auto', border: '0px', display: 'inline-block', marginLeft: '15px', cursor: 'pointer'}}>
                            <i className="material-icons">backup</i>
                        </Dropzone>
                        <SelectField
                            floatingLabelText="Language"
                            errorText={!this.state.definitionLanguage && this.state.submitted? 'Definition language could not be empty' : null}
                            ref={(input)=> this.definitionLanguage = input}
                            value={this.state.definitionLanguage}
                            onChange={(event, index, value)=>{this.setState({definitionLanguage: value})}}
                            style={{display: 'block'}}
                            >
                            <MenuItem value={"en"} primaryText="English" />
                            <MenuItem value={"ja"} primaryText="Japanese" />
                            <MenuItem value={"vi"} primaryText="Vietnamese" />
                        </SelectField>
                        <br />
                    </Col>
                </Row>
                {lists}
                <Row >
                    <Col sm={12}>
                        <RaisedButton
                            label="Add more"
                            onTouchTap={this.handleAddMore}
                            style={{marginTop: '20px'}}
                            secondary={true}
                        />
                    </Col>
                </Row>
            </Grid>
            <Snackbar
                open={this.state.showErrorMessage}
                message="Upload error"
                autoHideDuration={4000}
            />
        </div>;
    }
}

export default StudyForm;