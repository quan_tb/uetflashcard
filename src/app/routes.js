module.exports = {
    ROUTES: {
        INDEX: '/',
        CREATE: '/create',
        VIEW_SET_BASE: '/studySet/',
        VIEW_SET_DETAIL: '/studySet/:setId',
        FLASHCARD: '/studySet/:setId/flashcard',
        TEST_FLASHCARD: '/studySet/:setId/test',
        LEARN: '/studySet/:setId/learn',
        GAME: '/studySet/:setId/game',
        EDIT_SET: '/edit/',
        EDIT_SET_PARAMS: '/edit/:setId',
        MY_LEARNING: '/myLearning',
        USER_PROFILE_BASE: '/profile/',
        USER_PROFILE_DETAIL: '/profile/:userId',
        CHAT: '/chat'
    },

    ROUTES_REGEX: /^(\/(profile|create|edit|myLearning|chat|studySet|\/.))/
}