import React, {Component} from 'react';
import Avatar from "material-ui/Avatar";
import RaisedButton from 'material-ui/RaisedButton';
import request from 'superagent';
import Dialog from 'material-ui/Dialog';
import {browserHistory, Link} from 'react-router';
import routes from './routes';
import config from "./config";
import $ from 'jquery';

export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            usersOnline: [],
            statusGame: 'pre',
            historyGame: [],
            open: false,
            player: [],
            countDownTime : config.timeOutGameRequest,
            contentGame: [],
            countDownGame: config.timeOutContentGame,
            countQuestion: 0,
            scoreGame: 0,
            scorePlayer: 0,
            clicked: false,
            fromAns: [],
            toAns: []
        };
        this.timerContentGame = this.timerContentGame.bind(this)
    }

    getListFriend() {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        request
            .get('/api/v1/user')
            .set('QUIZ-TOKEN', quizToken)
            .end(function (err, res) {
                if (err) {
                } else {
                    that.setState({
                        usersOnline : res.body.data
                    });
                }
            });
    }

    componentDidMount(){
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        request
            .get('/api/v1/history')
            .set('QUIZ-TOKEN', quizToken)
            .end((err, res) => {
                if(err) {
                    Materialize.toast(err, 5000);
                    console.log('hic');
                } else {
                    that.setState({historyGame: res.body.data});
                }
            });
        window.socketVar.on('change status', (data) => {
            if(data['userId'] !== this.props.loggedUser._id) {
                this.getListFriend();
            }
        });
        window.socketVar.on('go to game', (data) => {
            that.setState({
                contentGame : data['contentGame'],
                statusGame: 'inPlay',
                open: false,
                player: data['player']
            });
           clearInterval(this.state.intervalId);
            var interval = setInterval(this.timerContentGame,100);
            that.setState({intervalQuestion: interval});
        });
        window.socketVar.on('receive score', (data) => {
            this.setState({scorePlayer: data['scoreGame']});
        });
        window.socketVar.on('receive result game', (data) => {
            this.setState({toAns: data['toAns']});
        });
       this.getListFriend();
    }

    timerContentGame() {
        let quizToken = window.localStorage.getItem('quizToken');
        this.setState({
            countDownGame: this.state.countDownGame - 1
        });
        if(this.state.countDownGame <= 0) {
            if(this.state.countQuestion < 4) {
                this.setState({
                    countQuestion: this.state.countQuestion + 1,
                    countDownGame: config.timeOutContentGame,
                    clicked: false
                });
            } else {
                clearInterval(this.state.intervalQuestion);
                this.setState({
                    countDownGame: 150,
                    result: true
                });
                window.socketVar.emit('send result game', {toAns: this.state.fromAns, playerId: this.state.player._id});
                if(this.state.scoreGame > this.state.scorePlayer) {
                    request
                        .post('/api/v1/game')
                        .set('QUIZ-TOKEN', quizToken)
                        .send({toId: this.state.player._id})
                        .end(function (err, res) {
                            if(err) {
                                Materialize.toast(err, 5000);
                            } else {
                                if(res.body.status) {
                                    Materialize.toast("History game is saved", 5000);
                                }
                            }
                        });
                }
            }
            $(function(){
                for(var i = 0; i < 4; i++){
                    $('#answer' + i).css('background-color', 'white');
                }
            });
        }
    }

    sendPlayRequest(item){
        window.socketVar.emit('create game request', {toId: item._id, fromUser: this.props.loggedUser, setId: this.props.params.setId});
        var interval = setInterval(this.timer,1000);
        this.setState({
            open: true,
            player: item,
            intervalId: interval
        });
        window.socketVar.on('response ignore request', (data) => {
            clearInterval(interval);
           this.setState({
               open: false,
               countDownTime: config.timeOutGameRequest
           });
            Materialize.toast(item.name + ' ignored your game request!', 5000);
        });
    }

    timer = () => {
        this.setState({
            countDownTime: this.state.countDownTime - 1
        });
        if(!this.state.open) clearInterval(this.state.intervalId);
        if(this.state.countDownTime <= 0) {
            clearInterval(this.state.intervalId);
            this.setState({
                open: false,
                countDownTime: config.timeOutGameRequest
            });
            Materialize.toast('No response for user', 5000);
        }
    };

    onAnswer = (answer, id) => {
        var that = this;
        if(!this.state.clicked) {
            if(answer == this.state.contentGame[this.state.countQuestion].definition) {
                this.setState({
                    scoreGame: this.state.scoreGame + this.state.countDownGame*10,
                    fromAns: this.state.fromAns.concat([true])
                }, () => {
                    window.socketVar.emit('update score', {scoreGame: this.state.scoreGame, playerId: this.state.player._id});
                });
                $(function(){
                    $('#'+id).css('background-color', '#1de9b6');
                });
            } else {
                this.setState({
                   fromAns: this.state.fromAns.concat([false])
                });
                $(function(){
                    $('#'+id).css('background-color', '#ef5350');
                    that.state.contentGame[that.state.countQuestion].randomAnswer.forEach((value, key) => {
                        if (value == that.state.contentGame[that.state.countQuestion].definition) {
                            $('#answer'+key).css('background-color', '#1de9b6');
                        }
                    });
                });
            }
            this.setState({
                clicked: true
            });
        }

    };

    handleClose = () => {
        this.setState({open: false});
    };

    render() {
        var that = this;
        const listUserOnline = this.state.usersOnline.map((item) =>
            <tr key={item._id}>
                <td>
                    <Avatar size={30} src={item.photo} />
                </td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>
                    <RaisedButton label="Send play request" primary={true} onClick={this.sendPlayRequest.bind(this, item)} />
                </td>
            </tr>
        );
        const resultGame = this.state.contentGame.map((item, index) =>
            <tr key={index}>
                <td>
                    <p><b>{item.term}</b></p>
                    <p><b>{item.definition}</b></p>
                </td>
                <td style={{maxWidth: '150px'}} className="center">{(that.state.fromAns[index]) ? <i className="material-icons" style={{color : '#4caf50'}}>check_circle</i> : <i className="material-icons" style={{color : '#ef5350'}}>highlight_off</i>}</td>
                <td style={{maxWidth: '150px'}} className="center">{(that.state.toAns[index]) ? <i className="material-icons" style={{color : '#4caf50'}}>check_circle</i> : <i className="material-icons" style={{color : '#ef5350'}}>highlight_off</i>}</td>
            </tr>
        );
        const historyGame = this.state.historyGame.map((item, index) =>
            (item.player1.id._id == that.props.loggedUser._id) ?
            <div className="row" key={index}>
                <div className="col s2 offset-s2">
                    <Avatar size={30} src={item.player1.id.photo} />
                    <p>You</p>
                </div>
                <div className="col s2 blue-text">
                    <p className="large">{item.player1.win} : {item.player2.win}</p>
                </div>
                <div className="col s4">
                    <Avatar size={30} src={item.player2.id.photo} />
                    <p>{item.player2.id.name}</p>
                </div>
            </div> :
            <div className="row" key={index}>
                <div className="col s2 offset-s2">
                    <Avatar size={30} src={item.player2.id.photo} />
                    <p>You</p>
                </div>
                <div className="col s2 bold blue-text">
                    <p className="large">{item.player2.win} : {item.player1.win}</p>
                </div>
                <div className="col s4">
                    <Avatar size={30} src={item.player1.id.photo} />
                    <p>{item.player1.id.name}</p>
                </div>
            </div>
        );
        return (
            <div className="container">
                { (this.state.statusGame == 'pre') &&
                <div>
                <div className="row">
                    <div className="s12">
                        <h5 style={{display: 'inline-block'}}>List user online to play</h5>
                        <RaisedButton style={{marginLeft: 10}} secondary={true} label="Back"
                                      onTouchTap={() => browserHistory.push(routes.ROUTES.VIEW_SET_BASE + this.props.params.setId)}/>
                        <table style={{marginTop: '20px'}}>
                            <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Request</th>
                            </tr>
                            </thead>
                            <tbody>
                            {listUserOnline}
                            </tbody>
                        </table>
                        <Dialog
                            title="Waiting a response from user"
                            modal={false}
                            open={this.state.open}
                            onRequestClose={this.handleClose}
                        >
                            <div className="row" style={{marginTop: '30px'}}>
                                <div className="col s5">
                                    <div className="card-panel grey lighten-5 z-depth-1">
                                        <div className="row" style={{marginBottom: '0px'}}>
                                            <div className="col s4">
                                                { this.props.loggedUser &&
                                                <img src={this.props.loggedUser.photo} alt="" className="circle responsive-img" />
                                                }
                                            </div>
                                            <div className="col s8">
                                                { this.props.loggedUser &&
                                                <p className="black-text">
                                                    {this.props.loggedUser.name}
                                                </p>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col s2 center">
                                    <p className="circle blue white-text" style={{display: 'inline-block', padding: '20px'}}>VS.</p>
                                </div>
                                <div className="col s5">
                                    <div className="card-panel grey lighten-5 z-depth-1">
                                        <div className="row" style={{marginBottom: '0px'}}>
                                            <div className="col s4">
                                                <img src={this.state.player.photo} alt="" className="circle responsive-img" />
                                            </div>
                                            <div className="col s8">
                                                <p className="black-text">
                                                    {this.state.player.name}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p style={{textAlign: 'center', margin: '0px', fontSize: '25px', color: '#1e88e5'}}><b>{this.state.countDownTime}</b></p>
                        </Dialog>
                    </div>
                </div>
                <div className="row">
                    <div className="col s12">
                        <h5>History Game</h5>
                        {historyGame}
                    </div>
                </div>
                </div>
                }
                {
                    (this.state.statusGame == 'inPlay') &&
                    <div id="interval-processing">
                        { (this.state.countDownGame <= 100) &&
                        <div className="progress">
                            <div className="determinate" style={{width: this.state.countDownGame + '%'}}></div>
                        </div>
                        }
                        <div className="row" style={{marginTop: '30px'}}>
                            <div className="col s3 offset-s1">
                                <div className="card-panel grey lighten-5 z-depth-1">
                                    <div className="row" style={{marginBottom: '0px'}}>
                                        <div className="col s4">
                                            <img src={this.props.loggedUser.photo} alt="" style={{maxWidth: '50px', maxHeight: '50px'}} className="circle responsive-img" />
                                        </div>
                                        <div className="col s8">
                                            <p className="black-text">
                                              {this.props.loggedUser.name}
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row center" style={{marginBottom: '0px'}}>
                                        <p style={{margin: '0px', fontSize: '22px', color: '#1e88e5'}}>{this.state.scoreGame}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col s2 offset-s1 center">
                                <p className="circle blue white-text" style={{display: 'inline-block', padding: '20px'}}>VS.</p>
                            </div>
                            <div className="col s3 offset-s1">
                                <div className="card-panel grey lighten-5 z-depth-1">
                                    <div className="row" style={{marginBottom: '0px'}}>
                                        <div className="col s4">
                                            <img src={this.state.player.photo} alt="" className="circle responsive-img" />
                                        </div>
                                        <div className="col s8">
                                          <p className="black-text">
                                              {this.state.player.name}
                                          </p>
                                        </div>
                                    </div>
                                    <div className="row center" style={{marginBottom: '0px'}}>
                                        <p style={{margin: '0px', fontSize: '22px', color: '#1e88e5'}}>{this.state.scorePlayer}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {   (!this.state.result) ?
                            <div className="row" style={{marginTop: '30px'}}>
                                <div className="col s12">
                                    <div className="card grey lighten-5 z-depth-1">
                                        <div className="card-content grey lighten-1">
                                            <span
                                                className="card-title blue-text"><b>{this.state.contentGame[this.state.countQuestion].term}:</b> </span>
                                            <div className="row answer">
                                                <div className="col s5">
                                                    <div className="card">
                                                        <div className="card-content" id="answer0"
                                                             onClick={this.onAnswer.bind(this, this.state.contentGame[this.state.countQuestion].randomAnswer[0], 'answer0')}>
                                                            <p><b>A. </b>{this.state.contentGame[this.state.countQuestion].randomAnswer[0]}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col s5 offset-s2">
                                                    <div className="card">
                                                        <div className="card-content" id="answer1"
                                                             onClick={this.onAnswer.bind(this, this.state.contentGame[this.state.countQuestion].randomAnswer[1], 'answer1')}>
                                                            <p><b>B. </b>{this.state.contentGame[this.state.countQuestion].randomAnswer[1]}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row answer">
                                                <div className="col s5">
                                                    <div className="card">
                                                        <div className="card-content" id="answer2"
                                                             onClick={this.onAnswer.bind(this, this.state.contentGame[this.state.countQuestion].randomAnswer[2], 'answer2')}>
                                                            <p><b>C. </b>{this.state.contentGame[this.state.countQuestion].randomAnswer[2]}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col s5 offset-s2">
                                                    <div className="card">
                                                        <div className="card-content" id="answer3"
                                                             onClick={this.onAnswer.bind(this, this.state.contentGame[this.state.countQuestion].randomAnswer[3], 'answer3')}>
                                                            <p><b>D. </b>{this.state.contentGame[this.state.countQuestion].randomAnswer[3]}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> :
                            <div className="row" style={{marginTop: '30px'}}>
                                <div className="col s8 offset-s2">
                                    <h4 className="center">
                                        {(this.state.scoreGame > this.state.scorePlayer) ? "You win !" : "You lose!"}
                                    </h4>
                                    <table className="bordered result">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th style={{width: '150px'}} className="center"><Avatar size={40} src={this.props.loggedUser.photo}/></th>
                                            <th style={{width: '150px'}} className="center"><Avatar size={40} src={this.state.player.photo}/></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {resultGame}
                                        </tbody>
                                    </table>
                                    <RaisedButton style={{float: 'right', marginTop: '40px'}} secondary={true} label="Back"
                                                  onTouchTap={() => browserHistory.push(routes.ROUTES.VIEW_SET_BASE + this.props.params.setId)}/>
                                </div>
                            </div>
                        }
                    </div>
                }
            </div>
        );
    }
}