import React, {Component} from 'react';
import {Card, CardActions, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Snackbar  from 'material-ui/Snackbar';
import AutoComplete from 'material-ui/AutoComplete';
import {Grid, Row, Col} from 'react-flexbox-grid';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import request from 'superagent';
import {browserHistory, Link} from 'react-router';
import Dotdotdot from 'react-dotdotdot';
import routes from './routes';
import Avatar from 'material-ui/Avatar';
const style = {
    width: '100%',
    textAlign: 'center',
    marginBottom: '20px'
};
const customText = {
    fontSize: '20px'
};

export default class extends Component {
    constructor(props){
        super(props);
        this.onNewRequest = this.onNewRequest.bind(this);
        this.state = {
            showErrorMessage: false,
            errorMessage: 'Could not delete',
            openDialog: false,
            selectedId: '',
            sets: [],
            dataSource: [],
            inputValue : ''
        }
    }

    openDialog = (id) => {
        this.setState({openDialog: true, selectedId: id});
    };

    handleCloseDialog = () => {
        this.setState({openDialog: false});
    };

    handleDeleteItem(){
        var that = this;
        if (this.state.selectedId){
            let quizToken = window.localStorage.getItem('quizToken');
            request
                .del('/api/v1/sets/' + this.state.selectedId)
                .set('QUIZ-TOKEN', quizToken)
                .end(function(err, res){
                    if (err) {
                        that.setState({showErrorMessage: true, errorMessage: 'Could not delete', selectedId: ''});
                        that.handleCloseDialog();
                    } else {
                        if (res.body.status){
                            var tempArr = that.state.sets.filter((item) => {return item._id != that.state.selectedId});
                            that.setState({selectedId: '', sets: tempArr});
                            that.handleCloseDialog();
                        }
                    }
                });
        }
    }

    componentDidMount(){
        this.getListSet();
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.updateSet) {
            this.getListSet();
        }
    }

    getListSet() {
        let quizToken = window.localStorage.getItem('quizToken');
        let that = this;
        request
            .get(this.props.url)
            .set('QUIZ-TOKEN', quizToken)
            .end(function(err, res){
                if (err) {
                    that.setState({showErrorMessage: true, errorMessage: 'Could not get data'});
                } else {
                    if (res.status){
                        const dataSearch = [];
                        res.body.data.sets.forEach((item) => {
                            dataSearch.push(item.title);
                        });
                        that.setState({dataSource: dataSearch});
                        console.log(res.body.data.sets);
                        that.setState({error: false, sets: res.body.data.sets});
                    }
                }
            });
    }

    onNewRequest(searchText) {
        let quizToken = window.localStorage.getItem('quizToken');
        let that = this;
        if(searchText == '') {
            request.get('api/v1/sets')
                    .set('QUIZ-TOKEN', quizToken)
                    .end(function(err,res) {
                        if(err) { Materialize.toast(err); }
                        else {
                            if(res.status) that.setState({error: false, sets: res.body.data.sets});
                        }
                    });
        } else {
            request
                .get('api/v1/sets/title/' + searchText)
                .set('QUIZ-TOKEN', quizToken)
                .end(function(err,res) {
                    if(err) {
                        Materialize.toast(err);
                    } else {
                        if(res.status) {
                            that.setState({error: false, sets: res.body.data});
                        }
                    }
                })
        }
    }

    render(){
        const lists = this.state.sets.map((item) =>
            <Col key={item._id} xs={12} sm={6} md={3} lg={3}>
                <Card  style={style}>
                    <CardMedia
                        overlay={<CardTitle title={<Dotdotdot clamp={1}>{item.title}</Dotdotdot>} titleStyle={customText}/>}
                    >
                        <div style={{width: "100%", paddingBottom: '75%', backgroundImage: (item.image ? 'url('+item.image+')' : 'url(/photo_holder.png)'), backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat:'no-repeat'}}>
                        </div>
                    </CardMedia>
                    <CardText>
                        <Dotdotdot clamp={1}>{item.description}</Dotdotdot>
                    </CardText>
                    {
                        (item.createdBy.name) &&
                        <CardText style={{minHeight: 30, padding: '0 20px 0 0'}}>
                            <Link to={routes.ROUTES.USER_PROFILE_BASE + item.createdBy._id} style={{float: 'right', display: 'inline-block', marginLeft: '5px', marginTop: '5px'}}>{item.createdBy.name}</Link>
                            <Avatar size={30} src={item.createdBy.photo} style={{float: 'right'}}/>
                        </CardText>
                    }
                    <CardActions>
                        <FlatButton style={{minWidth: 40}} primary={true} icon={<FontIcon className="fa fa-play" />} onTouchTap={()=>{browserHistory.push(routes.ROUTES.VIEW_SET_BASE + item._id)}} />
                        <FlatButton style={{minWidth: 40, display: this.props.homepage? 'none' : 'inline-block'}} secondary={true} icon={<FontIcon className="fa fa-pencil" />} onTouchTap={()=>{browserHistory.push(routes.ROUTES.EDIT_SET + item._id)}}  />
                        <FlatButton style={{minWidth: 40, display: this.props.homepage? 'none' : 'inline-block' }} onTouchTap={()=>{this.openDialog(item._id)}} icon={<FontIcon className="fa fa-trash" />} />
                    </CardActions>
                </Card>
            </Col>
        );
        const actions = [
            <FlatButton
                label="Yes"
                primary={true}
                onTouchTap={this.handleDeleteItem.bind(this)}
            />,
            <FlatButton
                label="No"
                primary={true}
                onTouchTap={this.handleCloseDialog}
            />
        ];
        return <Grid style={{marginTop: '20px'}}>
            {
            (this.props.url == '/api/v1/sets/') &&
            <Row>
                <AutoComplete
                    dataSource    = {this.state.dataSource}
                    onNewRequest = {this.onNewRequest}
                    onUpdateInput = {this.onNewRequest}
                    filter = {AutoComplete.caseInsensitiveFilter}
                    fullWidth = {true}
                    hintText = "Search sets"
                    style = {{marginBottom: '20px'}}
                />
            </Row>
            }
            <Row>{lists}</Row>
            <Row><Snackbar
                open={this.state.showErrorMessage}
                message={this.state.errorMessage}
                autoHideDuration={4000}
            /></Row>
            <Row>
                <Dialog
                    title="Are you sure you want to delete this item?"
                    contentStyle={{width: '50%'}}
                    actions={actions}
                    modal={false}
                    open={this.state.openDialog}
                >
                </Dialog>
            </Row>
        </Grid>;
    }
};