import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {browserHistory} from 'react-router';
import routes from './routes';
import $ from 'jquery';
import './libs/flashcard/jquery.circle';
import './libs/flashcard/flashcard';
import request from 'superagent';
import RaisedButton from 'material-ui/RaisedButton';
import NiceTextField from './components/NiceTextField';

export default class extends Component {
    constructor(){
        super();
        this.state = {
            progressArr: [],
            timeResponse: 0,
            questionCount: 0,
            showAnswer: false,
            showCountTimer: true
        };
        this.timer = this.timer.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    getNextCard(){
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        if (this.props.params.setId) {
            request
                .get('/api/v1/sets/' + this.props.params.setId + '/nextcard')
                .set('QUIZ-TOKEN', quizToken)
                .end(function (err, res) {
                    if (err) {
                    } else {
                        var data = res.body.data;
                        that.setState({
                            progressArr: data,
                            countDownTime: 100
                        }, () => {
                            console.log(that.state.progressArr);
                            if(!that.state.progressArr.length) {
                                clearInterval(that.state.intervalId);
                            }
                        });
                    }
                });
        }
    }

    componentDidMount() {
        clearInterval(this.state.intervalId);
        var interval = setInterval(this.timer,1000);
        this.setState({intervalId: interval}, () => {
            this.getNextCard();
        });

    }

    timer = () => {
        var that = this;
        this.setState({
            timeResponse: this.state.timeResponse + 1
        });
    };

    onAnswer(quality){
        var that = this;
        var progressId = $(this.container).find('.learn-flashcard').attr('data-progressId');
        let quizToken = window.localStorage.getItem('quizToken');
        request
            .post('/api/v1/progress/' + progressId)
            .set('QUIZ-TOKEN', quizToken)
            .send({quality: quality})
            .end(function (err, res) {
                if (err) {
                } else {
                    setTimeout(function () {
                        that.getNextCard();
                    }, 10)
                }
            });
    }

    onSubmitDefinition(key) {
        var that = this;
        var answer = this.answer.getValue();
        that.setState({
            showAnswer: true,
            showCountTimer: false
        });
        setTimeout(function() {
            var timeResponse = that.state.timeResponse - 5;
            if(answer == that.state.progressArr[key].pairObject.definition) {
                switch (true) {
                    case (timeResponse >= 0) && (timeResponse <= 20) :
                        that.onAnswer(5);
                        break;
                    case (timeResponse > 20) && (timeResponse <= 30) :
                        that.onAnswer(4);
                        break;
                    case (timeResponse > 30) && (timeResponse <= 40) :
                        that.onAnswer(3);
                        break;
                    case (timeResponse > 40) && (timeResponse <= 50) :
                        that.onAnswer(2);
                        break;
                    case (timeResponse > 50) && (timeResponse <= 60) :
                        that.onAnswer(1);
                        break;
                    case (timeResponse > 60) :
                        that.onAnswer(0);
                        break;
                }
            } else {
                that.onAnswer(0);
            }
            that.answer.setValue('');
            that.setState({
                showAnswer: false,
                timeResponse: 0,
                showCountTimer: true
            });
        },5000);
    }

    handleKeyPress(event) {
        var that = this;
        if(event.key == 'Enter'){
            that.onSubmitDefinition(0);
        }
    }

    componentWillUnmount(){
        clearInterval(this.state.intervalId);
        if (this.container){
            $(this.container).flashcard('destroy');
        }
    }

    render() {
        return <div ref={(d) => this.container = d} className={this.props.className}>
            <div className="container">
                <div className="row">
                    {this.state.progressArr.length ?
                    <div>
                    { (this.state.showCountTimer) &&
                        <div>
                            <p className="timer-count">{this.state.timeResponse}</p>
                        </div>
                    }
                    <div className="col s6 offset-s3">
                        <div className="learn-flashcard center" data-progressId = {this.state.progressArr[0].id}>
                            <ul id="deck" style={{marginBottom: '10px', width: '100%'}}>
                                <li className="cardf current" style={{width: '100%'}}>
                                    <div className="side_one">
                                        <p>{this.state.progressArr[0].pairObject.term}</p>
                                    </div>
                                </li>
                            </ul>
                            {/*<p>{this.state.progressArr[0].pairObject.term}</p>*/}
                            <NiceTextField ref={(input) => { this.answer = input; }} floatingLabelText="Enter definition" fullWidth={true} type="text" hintText="Answer the definition" onKeyPress={this.handleKeyPress}/><br />
                            <RaisedButton style={{marginLeft: 10, float:'right'}} secondary={true} label="Back"
                                          onTouchTap={() => browserHistory.push(routes.ROUTES.VIEW_SET_BASE + this.props.params.setId)}/>
                            <RaisedButton style={{marginLeft: 10, float:'right'}} primary={true} label="Answer"
                                          onClick={this.onSubmitDefinition.bind(this, 0)} />
                            {
                                (this.state.showAnswer) &&
                                    <p className="blue-text">Definition: <b>{this.state.progressArr[0].pairObject.definition}</b></p>
                            }
                        </div>
                    </div>
                    </div> :
                    <div className="center">
                        <p>You've done for now. Please come back later</p>
                        <RaisedButton style={{marginLeft: 10}} secondary={true} label="Back"
                        onTouchTap={() => browserHistory.push(routes.ROUTES.VIEW_SET_BASE + this.props.params.setId)}/>
                    </div>
                    }
                </div>
            </div>
        </div>;
    }
}