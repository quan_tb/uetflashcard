import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import routes from './routes';
import NiceTextField from './components/NiceTextField';
import request from 'superagent';
import {Grid, Row, Col} from 'react-flexbox-grid';
import RaisedButton from 'material-ui/RaisedButton';
import scrollToComponent from 'react-scroll-to-component';
import Utils from './libs/utils';
import moment from 'moment';
import {Bar} from 'react-chartjs-2';

export default class extends Component {
    constructor(){
        super();

        this.state = {
            studySet: [],
            title: '',
            description: '',
            correctCount: 0,
            incorrectCount: 0,
            notAnswer: 0,
            showResult: false,
            testResults: [],
            showAnswers: {},
            showChart: false
        };

        this.fields = {};
    }

    getTestResult (){
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        if (this.props.params.setId) {
            request
                .get('/api/v1/test/' + this.props.params.setId)
                .set('QUIZ-TOKEN', quizToken)
                .end(function (err, res) {
                    if (err) {
                    } else {
                        var data = res.body.data;
                        that.setState({
                            testResults: data,
                        });
                    }
                });
        }
    }

    componentDidMount() {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        if (this.props.params.setId) {
            request
                .get('/api/v1/sets/' + this.props.params.setId)
                .set('QUIZ-TOKEN', quizToken)
                .end(function (err, res) {
                    if (err) {
                    } else {
                        var doc = res.body.data;
                        that.setState({
                            studySet: Utils.shuffle(doc.sets),
                            setId: doc._id,
                            title: doc.title,
                            description: doc.description
                        });
                    }
                });
            that.getTestResult();
        }
    }

    onSubmitTest(){
        var that = this;
        this.setState({correctCount: 0, incorrectCount: 0, notAnswer: 0, showResult: false});
        var notAnswer = 0, correctCount = 0, incorrectCount = 0;
        var incorrectItems = {};
        Object.keys(this.fields).forEach((item) => {
            if (!that.fields[item].getValue()){
                that.fields[item].setState({errorText: 'Not Answer'});
                notAnswer++;
            } else {
                var filtered = that.state.studySet.filter((s) => {
                    return s.id == item;
                });

                if (filtered.length){
                    if (filtered[0].definition === that.fields[item].getValue()){
                        that.fields[item].setState({errorText: ''});
                        correctCount++;
                    } else {
                        that.fields[item].setState({errorText: 'Incorrect'});
                        incorrectCount++;
                        incorrectItems[item] = true;
                    }
                }
            }
        })
        
        if (!notAnswer){
            let quizToken = window.localStorage.getItem('quizToken');
            if (this.props.params.setId) {
                request
                    .post('/api/v1/test/' + this.props.params.setId)
                    .set('QUIZ-TOKEN', quizToken)
                    .send({correct: correctCount, incorrect: incorrectCount})
                    .end(function (err, res) {
                        if (err) {
                        } else {
                            var doc = res.body.data;
                            that.setState({
                                testResults: doc
                            });
                            that.setState({correctCount: correctCount, incorrectCount: incorrectCount, notAnswer: 0, showResult: true, showAnswers: incorrectItems});
                            setTimeout(()=> scrollToComponent(that.resultDiv), 100);
                        }
                    });
            }
        } else {
            that.setState({showResult: true, notAnswer: notAnswer, incorrectCount: incorrectCount, correctCount: correctCount, showAnswers: {}});
            setTimeout(()=> scrollToComponent(that.resultDiv), 100);
        }
    }

    onTryAgain(){
        var that = this;
        Object.keys(this.fields).forEach((item) => {
            that.fields[item].setState({errorText: ''});
            that.fields[item].setValue('');
        });

        that.setState({correctCount: 0, incorrectCount: 0, notAnswer: 0, showResult: false, showAnswers: {}})
    }

    showChartResult() {
        if(this.state.showChartResult) {
            this.setState({showChartResult: false});
        } else this.setState({showChartResult: true});
    }
    render() {
        const items = this.state.studySet.map((item, index) => {
            if (index < 20){
                return <div key={item.id}>
                    <h5 style={{marginBottom: 0}}>{item.term}</h5>
                    <NiceTextField ref={(input) => { this.fields[item.id] = input }} floatingLabelText="Answer" fullWidth={true} type="text" hintText="Enter your answer"/>
                    <br />
                    { this.state.showAnswers[item.id] && (<p style={{color: 'green', fontWeight: 700}}>Answer: {item.definition}</p>)}
                    <br />
                </div>
            }
        });
        const resultList = this.state.testResults.map((item) =>
            <tr key={item._id}>
                <td>{item.correct}</td>
                <td>{item.incorrect}</td>
                <td>{moment(item.createdAt).format('DD/MM/YYYY h:mm a')}</td>
            </tr>
        );
        const dataTime = [], dataCorrect = [];
        this.state.testResults.forEach((item) => { 
            dataTime.push(moment(item.createdAt).format('DD/MM/YYYY h:mm a'));
            dataCorrect.push(item.correct);
        });
        const dataChart = {
            labels: dataTime,
            datasets: [
                {
                  label: 'Correct answer',
                  backgroundColor: 'rgba(255,99,132,0.2)',
                  borderColor: 'rgba(255,99,132,1)',
                  borderWidth: 1,
                  hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                  hoverBorderColor: 'rgba(255,99,132,1)',
                  data: dataCorrect
                }
            ]
        };
        return <div>
            <div style={{marginBottom: 20}}>
                <Grid>
                    <Row>
                        <Col sm={12}>
                            <h5 style={{display: 'inline-block'}}>{this.state.title}</h5>
                            <RaisedButton style={{marginLeft: 10}} secondary={true} label="Back" onTouchTap={()=> browserHistory.push(routes.ROUTES.VIEW_SET_BASE + this.state.setId)} />
                            <p>{this.state.description}</p>
                            <hr/>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={6}>
                            {items}
                            <div>
                                <RaisedButton primary={true} label="Submit" onTouchTap={this.onSubmitTest.bind(this)} />
                                <RaisedButton style={{marginLeft: 10}} secondary={true} label="Try Again" onTouchTap={this.onTryAgain.bind(this)} />
                                <RaisedButton style={{marginLeft: 10}} primary={true} label="Done" onTouchTap={()=> browserHistory.push(routes.ROUTES.VIEW_SET_BASE + this.state.setId)} />
                            </div>
                        </Col>
                        <Col sm={6}>
                            {this.state.showResult && (
                            <div ref={(d) => this.resultDiv = d} style={{padding: 10}}>
                                <h3>Result:</h3>
                                <h4 style={{color: 'green', fontWeight: 700}}>Correct: {this.state.correctCount}</h4>
                                <h4 style={{color: 'red', fontWeight: 700}}>Incorrect: {this.state.incorrectCount}</h4>
                                <h4 style={{color: 'orange', fontWeight: 700}}>Not Answer: {this.state.notAnswer}</h4>
                                <br/>
                            </div>
                            )}
                            {
                                this.state.showChartResult ? 
                                <div>
                                    <Bar
                                      data={dataChart}
                                    />
                                </div> :
                                <div style={{padding: 10}}>
                                    <h3>Last results</h3>
                                    <table style={{width: '100%', textAlign: 'left'}} className="table">
                                        <thead>
                                        <tr>
                                            <th>Correct</th>
                                            <th>Incorrect</th>
                                            <th>Date time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {resultList}
                                        </tbody>
                                    </table>
                                </div>
                            }
                            <RaisedButton style={{marginLeft: '50px'}} primary={true} label="Switch Chart result" onTouchTap={this.showChartResult.bind(this)} />
                        </Col>
                    </Row>
                </Grid>
            </div>
        </div>;
    }
}