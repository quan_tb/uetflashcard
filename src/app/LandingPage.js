import React, {Component} from 'react';

class LandingPage extends Component {
    render() {
        return (
          <div>
              <div className="section no-pad-bot" id="index-banner">
                  <div className="container">
                      <h1 className="text_h center header cd-headline letters type">
                          <span>You want </span>
                          <span className="cd-words-wrapper waiting">
                            <b className="is-visible">studying</b>
                            <b>to be good ?</b>
                            <b>use Uet Flashcard !</b>
                           </span>
                      </h1>
                  </div>
              </div>
              <div id="intro" className="section scrollspy">
                  <div className="container">
                      <div className="row">
                          <div  className="col s12">
                              <h2 className="center header text_h2">
                                  <span className="span_h2"> Feature  </span>of Uet Flashcard
                              </h2>
                          </div>

                          <div  className="col s12 m4 l4">
                              <div className="center promo promo-example">
                                  <i className="mdi-image-flash-on" />
                                  <h5 className="promo-caption">Learning and test Flashcard</h5>
                                  <p className="light center">Uet Flashcard using modern althogrim to apply for Learn Flashcard, helps user learn Flascard with performance best.</p>
                              </div>
                          </div>
                          <div className="col s12 m4 l4">
                              <div className="center promo promo-example">
                                  <i className="mdi-social-group" />
                                  <h5 className="promo-caption">User vs User</h5>
                                  <p className="light center">Uet Flashcard have a feature to add friend, play game with Flashcard, and chat with friend.</p>
                              </div>
                          </div>
                          <div className="col s12 m4 l4">
                              <div className="center promo promo-example">
                                  <i className="mdi-hardware-desktop-windows" />
                                  <h5 className="promo-caption">Responsive</h5>
                                  <p className="light center">Uet Flashcard have responsive to mobile.</p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div className="parallax-container">
                  <div className="parallax"><img src="images/landing-page/parallax1.png" alt=""/></div>
              </div>
              <div className="section scrollspy" id="team">
                  <div className="container">
                      <h2 className="header text_b"> Author </h2>
                      <div className="row">
                          <div className="col s12 m4 offset-m4">
                              <div className="card card-avatar">
                                  <div className="waves-effect waves-block waves-light">
                                      <img className="activator" src="images/landing-page/avatar2.png" />
                                  </div>
                                  <div className="card-content">
                                      <span className="card-title activator grey-text text-darken-4">Trần Bá Quân<br/>
                                        <small><em><a className="red-text text-darken-1" href="#">Developer</a></em></small></span>
                                      <p>
                                          <a className="blue-text text-lighten-2" href="https://www.facebook.com/zzbynzz95">
                                              <i className="fa fa-facebook-square" />
                                          </a>
                                          <a className="blue-text text-lighten-2" href="https://plus.google.com/u/0/109901992370366271818">
                                              <i className="fa fa-google-plus-square" />
                                          </a>
                                      </p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        );
    }
}

export default LandingPage;