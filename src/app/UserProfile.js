import React, {Component} from 'react';
import request from 'superagent';
import {Link} from 'react-router';
import routes from './routes';
import {Grid, Row, Col} from 'react-flexbox-grid';
import InlineEdit from './InlineEdit';
import RaisedButton from 'material-ui/RaisedButton';
import './libs/custom-min';
import $ from 'jquery';
import Avatar from "material-ui/Avatar";
import moment from 'moment';

export default class extends Component {
    constructor(props) {
        super(props);
        this.dataChanged = this.dataChanged.bind(this);
        this.state = {
            userDetail: {},
            listFriends: [],
            listStudySets: [],
            name: '',
            sendRequest: '',
            loggedId: ''
        };
    }

    dataChanged(data) {
        let quizToken = window.localStorage.getItem('quizToken');
        var that = this;
        var postData = {
            userId: this.state.userDetail.id,
            name: data.name
        };
        if (this.state.userDetail.id) {
            request
                .post('/api/v1/user/' + this.state.userDetail.id)
                .set('QUIZ-TOKEN', quizToken)
                .send(postData)
                .end(function (err, res) {
                    if (err) {
                        Materialize.toast(err, 4000);
                    } else {
                        that.setState({...data});
                        that.props.updateName();
                        Materialize.toast("Edited successfully!", 3000);
                    }
                });
        }
    }

    customValidateText(text) {
        return (text.length > 0 && text.length < 64);
    }

    componentDidMount() {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        if (this.props.params.userId) {
            this.getListStudySet(this.props.params.userId);
            this.getUserProfile(this.props.params.userId);
            request
                .get('/api/v1/request/checkPending/' + this.props.params.userId)
                .set('QUIZ-TOKEN', quizToken)
                .end(function(err, res) {
                    that.setState({loggedId: res.body.loggedId});
                    if(err) {
                    } else {
                        if(res.body.isFriend) {
                            that.setState({
                                sendRequest: res.body.isFriend
                            });
                        }
                    }
                })
        }
        window.socketVar.on('response friend', (data) => {
            this.getUserProfile();
            this.setState({sendRequest: 'friend'})
        });
        window.socketVar.on('response ignore', (data) => {
           this.setState({sendRequest: ''})
        });
        window.socketVar.on('change status', (data) => {
            if(data['userId'] !== this.props.loggedUser._id) {
              this.getUserProfile();
            }       
        });
        $(function(){
            if ($('#tabs-swipe').length) {
                $('#tabs-swipe').tabs({ 'swipeable': true });
            }
        });
    }

    getListStudySet(userId) {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        request
            .get('/api/v1/sets/user/' + userId)
            .set('QUIZ-TOKEN', quizToken)
            .end(function(err, res) {
                if(err) {
                    Materialize.toast(err, 5000);
                } else {
                    var set = res.body.data;
                    that.setState({
                       listStudySets: set
                    });
                }
            })
    }

    getUserProfile(userId) {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        request
            .get('/api/v1/user/' + userId)
            .set('QUIZ-TOKEN', quizToken)
            .end(function (err, res) {
                if (err) {
                } else {
                    var doc = res.body.data;
                    that.setState({
                        userDetail: {
                            name: doc.name,
                            email: doc.email,
                            photo: doc.photo,
                            id: doc._id
                        },
                        name: doc.name,
                        listFriends: doc.listFriends
                    });
                }
            });
    }

    handleAddFriend() {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        let data = {
            fromId : this.props.loggedUser._id,
            toId: this.state.userDetail.id
        };
        request
            .post('/api/v1/request/create')
            .set('QUIZ-TOKEN', quizToken)
            .send(data)
            .end(function(err, res) {
               if(err) {
                   Materialize.toast(err, 5000);
               } else {
                   Materialize.toast("Send friend request successfully!", 5000);
                   that.setState({
                      sendRequest: 'pending'
                   });
                   window.socketVar.emit("create friend", {toFriendId: that.state.userDetail.id, fromFriendName: that.props.loggedUser.name});
               }
            });
    }

    handleUnfriend() {
        var that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        request
            .post('/api/v1/request/unfriend')
            .set('QUIZ-TOKEN', quizToken)
            .send({toId: this.props.params.userId})
            .end(function(err, res) {
               if(err) {
                   Materialize.toast(err, 5000);
               } else {
                    if(res.status) {
                        Materialize.toast("Unfriend successfully !", 5000);
                        that.setState({
                           sendRequest: ''
                        });
                        window.socketVar.emit("unfriend", {toFriendId: that.props.params.userId, fromFriendName: that.props.loggedUser.name});
                    }
               }
            });
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.params.userId) {
            var that = this;
            let quizToken = window.localStorage.getItem('quizToken');
            if (nextProps.params.userId) {
                this.getListStudySet(nextProps.params.userId);
                this.getUserProfile(nextProps.params.userId);
                request
                    .get('/api/v1/request/checkPending/' + nextProps.params.userId)
                    .set('QUIZ-TOKEN', quizToken)
                    .end(function(err, res) {
                        that.setState({loggedId: res.body.loggedId});
                        if(err) {
                        } else {
                            if(res.body.isFriend) {
                                that.setState({
                                    sendRequest: res.body.isFriend
                                });
                            }
                        }
                    })
            }
        }
    }

    render() {
        const list = this.state.listFriends.map((item) =>
            <tr key={item._id}>
                <td>
                  <Avatar size={30} src={item.photo} />                  
                </td>
                <td><Link to={routes.ROUTES.USER_PROFILE_BASE + item._id}>{item.name}</Link></td>
                <td>{item.email}</td>
                <td>
                   {
                     (item.online) ? <p><span className="online"></span>Online</p> : <p><span className="offline"></span>Offline</p>
                   }
                </td>
            </tr>
        );
        const listStudy = this.state.listStudySets.map((item, key) =>
            <tr key={item._id}>
                <td>{key + 1}</td>
                <td><Link to={routes.ROUTES.VIEW_SET_BASE + item._id}>{item.title}</Link></td>
                <td>{item.description}</td>
                <td>{item.sets.length}</td>
                <td>{moment(item.updatedAt).format('DD/MM/YYYY, hh:mm:ss a')}</td>
            </tr>
        );
        return(
          <div>
              <Grid>
                  <div id="profile-page">
                      <div id="profile-page-header" className="card">
                          <div className="card-image waves-effect waves-block waves-light">
                              <img className="activator" src={'/images/user-profile-bg.jpg'} alt="user background"/>
                          </div>
                          <figure className="card-profile-image">
                              <img src={this.state.userDetail.photo} alt="profile image" style={{width: '100px'}} className="circle z-depth-2 responsive-img activator"/>
                          </figure>
                          <div className="card-content">
                              <div className="row">
                                  <div className="col s4 offset-s2">
                                      { (this.state.loggedId !== this.state.userDetail.id) ?
                                          <h4 className="card-title grey-text text-darken-4">
                                              {this.state.name}
                                          </h4> :
                                          <InlineEdit
                                              className="card-title grey-text text-darken-4"
                                              validate={this.customValidateText}
                                              activeClassName="editing card-title grey-text text-darken-4"
                                              text={this.state.name}
                                              paramName="name"
                                              change={this.dataChanged}
                                          />
                                      }

                                      <p className="medium-small grey-text">{this.state.userDetail.email}</p>
                                  </div>
                                  { (this.state.loggedId !== this.state.userDetail.id) ?
                                      <div className="col s4 offset-s2">
                                          {
                                              (this.state.sendRequest == 'pending') &&
                                                  <RaisedButton label="Pending friend request" disabled={true} style={{marginTop: "30px"}} />
                                          }
                                          {
                                              (this.state.sendRequest == '') &&
                                                  <RaisedButton label="Add friend" primary={true} onClick={this.handleAddFriend.bind(this)} style={{marginTop: "30px"}} />
                                          }
                                          {
                                              (this.state.sendRequest == 'friend') &&
                                                  <RaisedButton label="Unfriend" secondary={true} onClick={this.handleUnfriend.bind(this)} style={{marginTop: "30px"}} />
                                          }
                                      </div>
                                      : <div></div>
                                  }

                              </div>
                          </div>
                      </div>
                      <ul id="tabs-swipe" className="tabs">
                          <li className="tab col s6"><a href="#list-request">List Study Set</a></li>
                          <li className="tab col s6"><a href="#list-friend">List Friend</a></li>
                      </ul>
                      <div className="tabs-content">
                          <div id="list-friend" className="col s12 white">
                              <div className="row">
                                  <div className="col s10 offset-s1">
                                      {this.state.listFriends.length == 0 ?
                                        <p className="center">List friends is empty.</p> :
                                          <table>
                                              <thead>
                                              <tr>
                                                  <th></th>
                                                  <th>Name</th>
                                                  <th>Email</th>
                                                  <th>Online</th>
                                              </tr>
                                              </thead>
                                              <tbody>
                                              {list}
                                              </tbody>
                                          </table>
                                      }

                                  </div>
                              </div>
                          </div>
                          <div id="list-request" className="col s12 white">
                              <div className="row">
                                  <div className="col s10 offset-s1">
                                      {this.state.listStudySets == 0 ?
                                        <p className="center">List study sets is empty.</p> :
                                          <table>
                                              <thead>
                                                  <tr>
                                                        <th>No</th>
                                                        <th>Title</th>
                                                        <th>Description</th>
                                                        <th>Term count</th>
                                                        <th>Updated at</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                              {listStudy}
                                              </tbody>
                                          </table>
                                      }
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </Grid>
          </div>
        );
    }
}