import React, {Component} from 'react';
import NiceTextField from './components/NiceTextField';
import request from 'superagent';

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            email: '',
            message: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const that = this;
        let quizToken = window.localStorage.getItem('quizToken');
        let data = {
            name: this.state.firstName,
            email: this.state.email,
            message: this.state.message
        };
        request
            .post('/api/v1/contact')
            .send(data)
            .end(function(err, res) {
                if(err) {
                    Materialize.toast(err, 5000);
                } else {
                    if(res.body.status) {
                        Materialize.toast('Message sent to admin ! We will contact you soon', 5000);
                        that.setState({
                            firstName: '',
                            email: '',
                            message: ''
                        });
                    }
                }
            });
    }

    render() {
        return (
            <footer id="contact" className="page-footer default_color scrollspy" style={{marginTop: '20px', width: '100%'}}>
                <div className="container">
                <div className="row">
                    <div className="col l6 s12">
                        <div className="row" style={{marginBottom: '0px'}}>
                            <div className="col s12">
                                <h5 className="white-text">Contact us</h5>
                            </div>
                        </div>
                        <form className="col s12" onSubmit={this.handleSubmit}>
                            <div className="row">
                                <div className="input-field col s6">
                                    <i className="mdi-action-account-circle prefix white-text" />
                                    <input id="icon_prefix" 
                                        value={this.state.firstName}
                                        onChange={this.handleChange}
                                        name="firstName" 
                                        type="text" 
                                        className="validate white-text"/>
                                    <label htmlFor="icon_prefix" className="white-text">First Name</label>
                                </div>
                                <div className="input-field col s6">
                                    <i className="mdi-communication-email prefix white-text"/>
                                    <input id="icon_email" 
                                        value={this.state.email}
                                        onChange={this.handleChange}
                                        name="email" 
                                        type="email" 
                                        className="validate white-text"/>
                                    <label htmlFor="icon_email" className="white-text">Email</label>
                                </div>
                                <div className="input-field col s12">
                                    <i className="mdi-editor-mode-edit prefix white-text"/>
                                    <textarea id="icon_prefix2"
                                        value={this.state.message}
                                        onChange={this.handleChange}
                                        name="message" 
                                        className="materialize-textarea white-text" />
                                    <label htmlFor="icon_prefix2" className="white-text">Message</label>
                                </div>
                                <div className="col offset-s7 s5">
                                    <button className="btn waves-effect waves-light red darken-1" type="submit">Submit
                                        <i className="mdi-content-send right white-text"/>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="col l3 s12">
                        <h5 className="white-text">uetflashcard.me</h5>
                        <ul>
                            <li><a className="white-text" href="http://uetflashcard.me">Home</a></li>
                            <li><a className="white-text" href="http://uetflashcard.me">Blog</a></li>
                        </ul>
                    </div>
                    <div className="col l3 s12">
                        <h5 className="white-text">Social</h5>
                        <ul>
                            <li>
                                <a className="white-text" href="https://www.facebook.com/zzbynzz95">
                                    <i className="small fa fa-facebook-square white-text"/> Facebook
                                </a>
                            </li>
                            <li>
                                <a className="white-text" href="https://github.com/zzbynzz">
                                    <i className="small fa fa-github-square white-text"/> Github
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                </div>
                <div className="footer-copyright default_color">
                    <div className="container">
                        Made by <a className="white-text" href="http://facebook.com/zzbynzz95">Tran Ba Quan</a>.
                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer;