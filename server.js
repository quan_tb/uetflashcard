var restify = require('restify');
var mongoose = require('mongoose');
var env = process.env.NODE_ENV || 'development';
var config = require('./src/config/' + env);

var server = restify.createServer();
server.pre(restify.pre.sanitizePath());
server.use(restify.acceptParser(server.acceptable));
server.use(restify.authorizationParser());
server.use(restify.dateParser());
server.use(restify.queryParser());
server.use(restify.gzipResponse());
server.use(restify.bodyParser());
server.use(restify.CORS({credentials: true, origins: ['*'], headers: ['Access-Control-Allow-Origin: *']}));
server.use(restify.requestExpiry(config.server.requestExpiry));
server.use(restify.requestLogger());
server.use(restify.throttle(config.server.throttle));
mongoose.connect(config.mongodb.url, config.mongodb.options); // connect to our database

var io = require('socket.io').listen(server.server);
var socketFile = require('./socket.js')(io);
// Apply routes
require('./src/api/user.router').applyRoutes(server, '/api/v1');
require('./src/api/study.router').applyRoutes(server, '/api/v1');
require('./src/api/friend.router').applyRoutes(server, '/api/v1');

const staticRoutes = require('./src/app/routes');
server.get(staticRoutes.ROUTES_REGEX, restify.serveStatic({
    directory: './build/',
    file: 'index.html'
}));

server.get(/^((?!\/api\/v1\/).)*$/, restify.serveStatic({
    directory: './build/',
    default: 'index.html'
}));


server.listen(config.server.port, function () {
    console.log('%s started at %s://%s:%s', config.server.name, config.server.protocol, config.server.host, config.server.port);
});

module.exports = server;