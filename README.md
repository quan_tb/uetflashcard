## Installation

After cloning the repository, install dependencies:
```sh
cd uetflashcard
npm install
```

Now you can run your local server:
```sh
npm start
```

Now open new terminal and run:
```sh
node server.js
```

Server is located at http://localhost:9000