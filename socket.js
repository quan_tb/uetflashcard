var helper = require('./src/api/helper');
var users = {};

exports = module.exports = function(io) {
    io.on('connection', function(socket) {
        var userId = 0;
        socket.on('new user', (data) => {
            userId = data['userId'];
            users[data['userId']] = socket;
            helper.changeStatus(userId, true, (err, user) => {
                socket.broadcast.emit('change status', {userId: userId});
            });
        });

        socket.on('create friend', (data) => {
            users[data['toFriendId']].emit('send-friend-request', {fromFriend: data['fromFriendName']});
        });

        socket.on('accept request', (data) => {
            if(users[data['userFromId']]) {
                users[data['userFromId']].emit('response friend', {fromFriend: data['userTo']});
            }
            users[data['userToId']].emit('response friend', {fromFriend: data['userFrom']});
        });

        socket.on('ignore request', (data) => {
            if(users[data['userFromId']]) {
                users[data['userFromId']].emit('response ignore', {fromFriend: data['userTo']});
            }
        });

        socket.on('unfriend', (data) => {
            if(users[data['toFriendId']]) {
                users[data['toFriendId']].emit('noti-unfriend', {unfriendName: data['fromFriendName']});
            }
        });

        //game
        socket.on('create game request', (data) => {
            if(users[data['toId']]) {
                users[data['toId']].emit('send game request', {fromUser: data['fromUser'], setId: data['setId']});
            }

        });

        socket.on('ignore game request', (data) => {
            (users[data['userFrom']._id]) && users[data['userFrom']._id].emit('response ignore request', {userFrom: data['userFrom']});
        });

        socket.on('accept game request', (data) => {
            helper.generateContentGame(data['setId'], (err, content) => {
                (users[data['loggedUser']._id]) && users[data['loggedUser']._id].emit('go to game', {player: data['toUser'], contentGame: content});
                (users[data['toUser']._id]) && users[data['toUser']._id].emit('go to game', {player: data['loggedUser'], contentGame: content});
            });
        });

        socket.on('update score', (data) => {
            (users[data['playerId']]) && users[data['playerId']].emit('receive score', {scoreGame: data['scoreGame']});
        });

        socket.on('send result game', (data) => {
            (users[data['playerId']]) && users[data['playerId']].emit('receive result game', {toAns: data['toAns']});
        });

        socket.on('send chat', () => {
            socket.broadcast.emit('receive chat');
        });

        socket.on('disconnect', function(){
            helper.changeStatus(userId, false, (err, user) => {
                socket.broadcast.emit('change status', {userId: userId});
            });
        });
    });
};