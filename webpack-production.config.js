const webpack = require('webpack');
const path = require('path');
const buildPath = path.resolve(__dirname, 'build');
const nodeModulesPath = path.resolve(__dirname, 'node_modules');
const TransferWebpackPlugin = require('transfer-webpack-plugin');
const config = {
    entry: [path.join(__dirname, '/src/app/app.js')],
    // Render source-map file for final build
    devtool: 'source-map',
    // output config
    output: {
        path: buildPath, // Path of output file
        filename: 'app.js', // Name of output file
    },
    plugins: [
        // Define production build to allow React to strip out unnecessary checks
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        // Minify the bundle
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                // suppresses warnings, usually from module minification
                warnings: false,
            },
        }),
        // Allows error warnings but does not stop compiling.
        new webpack.NoErrorsPlugin(),
        // Transfer Files
        new TransferWebpackPlugin([
            {from: 'www'},
        ], path.resolve(__dirname, 'src')),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.$': 'jquery',
            'window.jQuery': 'jquery',
            "Hammer": "hammerjs/hammer",
            Modernizr: 'modernizr'
        })
    ],
    module: {
        loaders: [
            {
                test: /\.js$/, // All .js files
                loaders: ['babel-loader'],
                exclude: [nodeModulesPath],
            },
            {
                test: /\.css$/,
                loader: 'style!css?modules',
                include: /flexboxgrid/,
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader',
                exclude: /flexboxgrid/
            },
            {
                test: /\.(png|woff(2)?|eot|ttf|svg)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /[\\\/]src[\\\/]app[\\\/]libs[\\\/]plugin-min\.js$/,
                loader: "imports?this=>window!exports?window.Modernizr"
            },
            {
                test: require.resolve("./src/app/libs/materialize.js"),
                loader: "imports-loader"
            },
            {
                test: require.resolve("./src/app/libs/custom-min.js"),
                loader: "imports-loader"
            },
            {
                test: require.resolve("./src/app/libs/flashcard/jquery.circle.js"),
                loader: "imports-loader"
            },
            {
                test: require.resolve("./src/app/libs/flashcard/flashcard.js"),
                loader: "imports-loader"
            },
            {
                test: /\.modernizrrc.js$/,
                loader: "modernizr"
            },
            {
                test: /\.modernizrrc(\.json)?$/,
                loader: "modernizr!json"
            }
        ],
    },
    resolve: {
        alias: {
            modernizr$: path.resolve(__dirname, ".modernizrrc")
        }
    }
};
module.exports = config;