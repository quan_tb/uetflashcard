const webpack = require('webpack');
const path = require('path');
const buildPath = path.resolve(__dirname, 'build');
const nodeModulesPath = path.resolve(__dirname, 'node_modules');
const TransferWebpackPlugin = require('transfer-webpack-plugin');
const config = {
    // Entry points to the project
    entry: [
        'webpack/hot/dev-server',
        'webpack/hot/only-dev-server',
        path.join(__dirname, '/src/app/app.js'),
    ],
    // Server Configuration options
    devServer: {
        contentBase: 'src/www', // Relative directory for base of server
        devtool: 'eval',
        hot: true, // Live-reload
        inline: true,
        port: 9000, // Port Number
        host: 'localhost', // Change to '0.0.0.0' for external facing server,
        proxy: {
            '/api': {
                target: {
                    host: "localhost",
                    protocol: 'http:',
                    port: 9100
                }
            }
        }
    },
    devtool: 'eval',
    output: {
        path: buildPath, // Path of output file
        filename: 'app.js',
    },
    plugins: [
        // Enables Hot Modules Replacement
        new webpack.HotModuleReplacementPlugin(),
        // Allows error warnings but does not stop compiling.
        new webpack.NoErrorsPlugin(),
        // Moves files
        new TransferWebpackPlugin([
            {from: 'www'},
        ], path.resolve(__dirname, 'src')),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.$': 'jquery',
            'window.jQuery': 'jquery',
            "Hammer": "hammerjs/hammer",
            Modernizr: 'modernizr'
        })
    ],
    module: {
        loaders: [
            {
                test: /\.js$/, // All .js files
                loaders: ['babel-loader'],
                exclude: [nodeModulesPath],
            },
            {
                test: /\.css$/,
                loader: 'style!css?modules',
                include: /flexboxgrid/,
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader',
                exclude: /flexboxgrid/
            },
            {
                test: /\.(png|woff(2)?|eot|ttf|svg)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /[\\\/]src[\\\/]app[\\\/]libs[\\\/]plugin-min\.js$/,
                loader: "imports?this=>window!exports?window.Modernizr"
            },
            {
                test: require.resolve("./src/app/libs/materialize.js"),
                loader: "imports-loader"
            },
            {
                test: require.resolve("./src/app/libs/custom-min.js"),
                loader: "imports-loader"
            },
            {
                test: require.resolve("./src/app/libs/flashcard/jquery.circle.js"),
                loader: "imports-loader"
            },
            {
                test: require.resolve("./src/app/libs/flashcard/flashcard.js"),
                loader: "imports-loader"
            },
            {
                test: /\.modernizrrc.js$/,
                loader: "modernizr"
            },
            {
                test: /\.modernizrrc(\.json)?$/,
                loader: "modernizr!json"
            }
        ],
    },
    resolve: {
        alias: {
            modernizr$: path.resolve(__dirname, ".modernizrrc")
        }
    }
};
module.exports = config;